;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Image gallery

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun image-gallery ()
  "Display the image gallery"
  (log-message :debug "Displaying image gallery")
  (let* ((sessionid (tbnl:cookie-in "sessionid"))
         ;; Execute the search
         (images (rg-request-json
                   (rg-server tbnl:*acceptor*)
                   "/Files"
                   :sessionid sessionid
                   :parameters '(("SCinbound"
                                  . "/MediatypeTypes/image/MEDIATYPE_TYPE_OF/MediaTypes/*/MEDIA_TYPE_OF")
                                 ("SCattributes" . "title")))))
    (log-message :debug (format nil "Fetched image data ~A" images))
    (with-output-to-string (outstr)
      (simple-clwho-layout
        outstr
        :title "Gallery"
        :stylesheets '("display" "gallery")
        :javascripts '()
        ;; Is the client authorised to make changes?
        :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                     sessionid))
                        t)
        :navbar-items (navbar-items tbnl:*acceptor*)
        :content
        (cl-who:with-html-output-to-string
          (content-string nil
                          :prologue nil
                          :indent t)
          (:div
            :class "content"
            (:div :id "image-list"
                  ;; Search criteria
                  (:form :action "/image-gallery"
                         :method "get"
                         (:div :id "gallery-filter"
                               (:div :id "filter-button"
                                     (:input :id "filter-submit"
                                             :type "submit"
                                             :value "Filter images"))))
                  ;; Search results - the actual gallery
                  (loop for img in
                        (sort images
                              #'string<
                              :key #'(lambda (image) (gethash "ScUID" image)))
                        do (cl-who:htm
                             (:div :class "gallery-image"
                                   (:a :href (format nil "/display/Files/~A" (gethash "ScUID" img))
                                       (:img :class "gallery-image"
                                             :src (format nil "/files/v1/~A" (gethash "ScUID" img))))
                                   (:div :class "gallery-image-text"
                                         (princ (gethash "title" img) content-string))))))))))))
