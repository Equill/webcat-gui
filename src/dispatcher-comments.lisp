;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Add a comment and redirect to the main display page

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun comments ()
  "Manage comments on resources."
  (let ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid"))))
    (cond
      ;; Either read-only mode, or session not authorised
      ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
           (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                (not sessionid)))
       (return-client-error "Request not permitted."))
      ;; Empty comment - just ignore these and go back to the page.
      ((and (equal :POST (tbnl:request-method*))
            (or (null (tbnl:post-parameter "text"))
                (equal "" (tbnl:post-parameter "text"))))
       (tbnl:redirect (concatenate 'string "/display" (tbnl:post-parameter "path"))))
      ;; Happy path
      ((equal :POST (tbnl:request-method*))
       (let ((path (tbnl:post-parameter "path"))
             (text (tbnl:post-parameter "text"))
             (uuid (uuid:make-v4-uuid)))
         (multiple-value-bind (body status-code)
           (rg-post-json (rg-server *webcat-gui-acceptor*)
                         ;; Generate the URI
                         (format nil "~A/HAS_COMMENT/Comments" path)
                         :payload `(("ScUID" . ,(format nil "~A" uuid))
                                    ("text" . ,text))
                         :sessionid sessionid)
           (if (and (> status-code 199)
                    (< status-code 300))
             ;; Happy path: no errors
             (tbnl:redirect (concatenate 'string "/display" path))
             ;;
             (progn
               (log-message :warn (format nil "Failed to add comment: ~D - ~A"
                                          status-code body))
               (setf (tbnl:content-type*) "text/html")
               (setf (tbnl:return-code*) tbnl:+http-bad-request+)
               (with-output-to-string (outstr)
                 (simple-clwho-layout
                   outstr
                   :title "Failed to append comment"
                   :stylesheets '("upload")
                   ;; Is the client authorised to make changes?
                   :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                           (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                                sessionid))
                                   t)
                   :navbar-items (navbar-items tbnl:*acceptor*)
                   :content (cl-who:with-html-output-to-string
                              (content-string nil
                                              :prologue nil
                                              :indent t)
                              (:div :class "content"
                                    (:p "Sorry, but there was a problem with appending the comment.")
                                    (:p "Reason: " (princ body content-string)))))))))))
      ;; Fall back to the usual error response.
      (t
        (method-not-allowed)))))
