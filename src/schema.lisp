;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;; Fetching and constructing an internal representation of the Syscat server's schema.

(in-package #:webcat-gui)


(defun get-resourcetype-schema (server &key (resourcetype "") sessionid)
  "Request the resourcetype schema from the Syscat server,
   and return it as an alist."
  (declare (type rg-server server)
           (type (or null string) resourcetype)
           (type (or null string) sessionid))
  (if (equal resourcetype "")
    (log-message :debug "Retrieving the whole resourcetype schema")
    (log-message :debug (format nil "Retrieving the schema for resourcetype '~A'" resourcetype)))
  (rg-request-json server
                   (format nil "resourcetypes/~A" resourcetype)
                   :api "schema"
                   :sessionid sessionid))

(defun digest-resourcetype-schema (schema)
  "Take the JSON-format schema returned by get-resourcetype-schema,
  and return a hash-table whose keys are resourcetype names,
  and whose values are schema-rtype instances."
  (log-message :debug "Digesting the schema file received from the Syscat server")
  ;; Initialise the hash-table
  (let ((dict (make-hash-table :test #'equal)))
    ;; Generate a schema-rtype instance for each resourcetype in the list,
    ;; and insert it in the hash-table
    (mapcar (lambda (rtype)
              ;; Pre-extract the name, because we use it repeatedly
              (let ((name (gethash "name" rtype)))
                (log-message :debug (format nil "Digesting resourcetype '~A'" name))
                (setf (gethash name dict)
                      (make-instance
                        'schema-rtype
                        :name name
                        :dependent (gethash "dependent" rtype)
                        :description (gethash "description" rtype)
                        :attributes (mapcar #'make-schema-rtype-attrs (gethash "attributes" rtype))
                        :relationships (mapcar
                                         (lambda (rel)
                                           (log-message :debug (format nil "Adding relationship '~A'"
                                                                       (gethash "name" rel)))
                                           (make-instance
                                             'outbound-rels
                                             :name (gethash "name" rel)
                                             :description (gethash "description" rel)
                                             :cardinality (gethash "cardinality" rel)
                                             :reltype (gethash "reltype" rel)
                                             :target-types (gethash "target-types" rel)))
                                         (gethash "relationships" rtype))))))
            schema)
    ;; Return the hash-table
    dict))

(defun get-attrs (schema resourcetype)
  "Retrieve a list of attributes for a resourcetype.
   Arguments:
   - server = instance of rg-server struct
   - resourcetype = string
   Returns a list of schema-rtype-attrs structs."
  (declare (type hash-table schema)
           (type string resourcetype))
  (log-message :debug (format nil "Fetching attributes for resourcetype '~A'" resourcetype))
  (attributes (gethash resourcetype schema)))

(defun get-resourcetypes (schema)
  "Retrieve a sorted list of resourcetype names."
  (declare (type hash-table schema))
  (sort
    (loop for key being the hash-keys in schema
          collecting key)
    #'string<))

(defun get-outbound-rels (schema resourcetype &key denylist)
  "Return a list of all valid outgoing relationships that can be made from this resourcetype,
   in the form of `outbound-rels` instances.
   Includes relationships to or from the 'any' type.
   Client can optionally supply a list of relationships that shouldn't be searched for."
  (declare (type hash-table schema)
           (type string resourcetype)
           (type list denylist))
  (remove-if #'(lambda (candidate)
                 (member (name candidate) denylist :test #'equal))
             (append
               ;; Both from this exact type...
               (relationships (gethash resourcetype schema))
               ;; ...and from "any"
               (relationships (gethash "any" schema)))))

(defun get-linked-resources (server schema uri-parts &key sessionid)
  "Retrieve a list of all resources linked from the given one.
  - server: an rg-server object
  - uri-parts: the output of get-uri-parts.
  Return a list of `linked-resource` instances."
  (declare (type rg-server server)
           (type list uri-parts)
           (type (or null string) sessionid))
  ;; Sanity check: is this plausibly a path to a resource?
  (if (= (mod (length uri-parts) 3) 2)
    ;; Yes: carry on.
    ;; Get a list of the resources to which this resource has links.
    ;; Do this by finding out what valid outgoing relationships it has,
    ;; then asking what's at the end of each of those relationships.
    ;;
    ;; Get a list of the valid outgoing relationships from this type
    ;; Pre-fetch stuff
    (let* ((acc (list))
           (resourcetype (car (last uri-parts 2)))
           (rels (get-outbound-rels
                   schema
                   resourcetype
                   :denylist '("SC_CREATOR" "HAS_COMMENT"))))
      ;; Iterate over the outbound relationships that are valid from this resourcetype
      (mapcar
        #'(lambda (rel)
            ;; Iterate over the resourcetypes at the end of each of these relationships
            (mapcar
              (lambda (rtype)
                (log-message
                  :debug
                  (format nil "Getting ~A resources with relationship ~A from resource ~{/~A~}"
                          rtype (name rel) uri-parts))
                ;; Having fetched any/all resources meeting that description,
                ;; generate a linked-resource instance and return that
                (mapcar #'(lambda (res)
                            (push
                              (make-instance 'linked-resource
                                             :relationship (name rel)
                                             :target-type (if (equal "any" rtype) (gethash "type" res) rtype)
                                             :dependent-p (equal "dependent" (reltype rel))
                                             :uid (gethash "ScUID" res)
                                             :canonical-path (gethash "ScCanonicalpath" res))
                              acc))
                        ;; Request all instances of resources of this type on this relationship
                        (handler-case
                          (rg-request-json
                            server
                            (if (equal "any" rtype)
                              (format nil "~{/~A~}/~A" uri-parts (name rel))
                              (format nil "~{/~A~}/~A/~A" uri-parts (name rel) rtype))
                            :sessionid sessionid)
                          ;; Generic "something went boom
                          (error () (log-message :error "get-linked-resources failed to fetch something")))))
              (target-types rel)))
        rels)
      ;; Return the contents of the accumulator
      (nreverse acc))
    ;; This can't be a resource
    (progn
      (log-message :error (format nil "get-linked-resources failed: ~{/~A~} can't be a resource"
                                  uri-parts))
      ;; Return nil
      nil)))

(defgeneric fetch-schema (server)
            (:documentation "Fetch the schema from the Syscat server,
                            and return it as a hash-table suitable for injecting into the acceptor.")
            (:method ((server rg-server)) (digest-resourcetype-schema
                                             (get-resourcetype-schema server))))
