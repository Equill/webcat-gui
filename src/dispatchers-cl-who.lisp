;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

;; Common functions used by dispatchers


(defclass navbar-items ()
  ((item1-title :initarg :item1-title
                :reader item1-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 1.")
   (item1-target :initarg :item1-target
                 :reader item1-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink.")
   (item2-title :initarg :item2-title
                :reader item2-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 2.")
   (item2-target :initarg :item2-target
                 :reader item2-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink.")
   (item3-title :initarg :item3-title
                :reader item3-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 3.")
   (item3-target :initarg :item3-target
                 :reader item3-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink.")
   (item4-title :initarg :item4-title
                :reader item4-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 4.")
   (item4-target :initarg :item4-target
                 :reader item4-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink.")
   (item5-title :initarg :item5-title
                :reader item5-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 5.")
   (item5-target :initarg :item5-target
                 :reader item5-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink.")
   (item6-title :initarg :item6-title
                :reader item6-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 6.")
   (item6-target :initarg :item6-target
                 :reader item6-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink.")
   (item7-title :initarg :item7-title
                :reader item7-title
                :type (or null string)
                :initform nil
                :documentation "The string to place in the nav-bar for item 7.")
   (item7-target :initarg :item7-target
                 :reader item7-target
                 :type (or null string)
                 :initform nil
                 :documentation "The URI to place in this item's hyperlink."))
  (:documentation "Determines what's in each slot of the navbar."))

(defun navbar (outstr items &key path authorised-p)
  "Top-of-page navigation bar.
   outstr = output stream"
  (declare (type stream outstr)
           (type navbar-items items)
           (type list path)
           (type boolean authorised-p))
  (log-message :debug (format nil "Rendering navbar with path '~{/~A~}'." path))
  (cl-who:with-html-output
    (outstr nil
            :prologue nil
            :indent t)
    (cl-who:htm
      (:div :class "navbar"
       ;; Search link
       (:a :id "nav-item-1" :class "nav-item" :href "/search" "Search")
       ;; Link to the Edit page _if_ we have something to link to,
       ;; and if the client is authorised.
       (if (and path authorised-p)
           (cl-who:htm (:a :id "nav-item-2" :class "nav-item"
                        :href (format nil "/editresource~{/~A~}" path)
                        "Edit"))
           (cl-who:htm (:div :class "nav-item-2" :class "nav-item" "Edit")))
       ;; Create new item - only if the client is authorised
       (if authorised-p
           (cl-who:htm (:a :id "nav-item-3" :class "nav-item" :href "/create" "Create"))
           (cl-who:htm (:div :id "nav-item-3" :class "nav-item" "Create")))
       ;; Tasks or Blog, depending on what's available
       (cond
         ((and authorised-p
               (item4-title items)
               (item4-target items))
          (cl-who:htm (:a :id "nav-item-4" :class "nav-item"
                          :href (item4-target items)
                          (princ (item4-title items) outstr))))
         ((item4-title items)
          (cl-who:htm (:div :id "nav-item-4" :class "nav-item"
                            (princ (item4-title items) outstr))))
         (t
           (cl-who:htm (:div :id "nav-item-4" :class "nav-item"
                            (princ "Blank" outstr)))))
       ;; File upload - if the client is authorised
       (if authorised-p
           (cl-who:htm (:a :id "nav-item-5" :class "nav-item" :href "/files-upload" "Files"))
           (cl-who:htm (:div :id "nav-item-5" :class "nav-item" "Files")))
       ;; Image gallery
       (:a :id "nav-item-6" :class "nav-item" :href "/image-gallery?tags-exclude=nsfw" "Image gallery")
       ;; Login, etc.
       (:a :id "nav-item-7" :class "nav-item" :href "/management" "Management")))))

(defun simple-clwho-layout (outstr &key (title "Page title goes here")
                                   path   ; List of strings
                                   stylesheets
                                   javascripts
                                   content
                                   authorised-p
                                   navbar-items)
  "Standard layout for ediing content, using cl-who.
  Content must be wrapped in a (:div) or other similar container."
  (declare (type string title)
           (list path stylesheets))
  (let ((resourcetype (car (last path 2)))
        (uid (car (last path))))
    (cl-who:with-html-output
      (outstr nil
              :prologue t
              :indent t)
      (:html
        (:head
          (:title (princ title outstr))
          ;; Stylesheets
          (loop for sheet in (append '("common") stylesheets)
                do (cl-who:htm (:link :rel "stylesheet"
                                      :type "text/css"
                                      :href (format nil "/static/css/~A.css" sheet))))
          ;; Javascript
          (loop for script in (append '("jquery-3.6.0" "common") javascripts)
                do (cl-who:htm (:script :src (format nil "/static/js/~A.js" script)))))
        (:body
          (:div
            :class "default-grid"
            (:h1 :id "title" (format outstr "~A~A"
                                     (or title (uid-to-title uid))
                                     (if resourcetype
                                       (format nil " (~A)" resourcetype)
                                       "")))
            (navbar outstr navbar-items
                    :path path
                    :authorised-p authorised-p)
            ;; Body-content
            (princ content outstr)
            (:p :class "footer" "Powered by <a class='footer' href='https://www.sysc.at/'>Syscat</a>")))))))


(defun detailed-clwho-layout
  (outstr &key (title "Page title goes here")
          stylesheets
          javascripts
          path ; Path to the resource, as a list of strings
          content
          outbound-links ; List of (relationships . URI) dotted pairs
          dependent-resources  ; List of dependent resources
          comments       ; List of `comment` objects
          authorised-p   ; Boolean: is the client authorised to make changes?
          navbar-items   ; `navbar-items` object
          )
  "Standard layout setup using cl-who. Content must be wrapped in a (:div) or other similar container."
  (declare (type string title)
           (list outbound-links comments))
  (log-message :debug (format nil "Rendering detailed layout for resource ~{/~A~}" path))
  (let* ((resourcetype (car (last path 2)))
         (uid (car (last path)))
         (title-string (format nil "~A~A"
                               (or title (uid-to-title uid))
                               (if resourcetype
                                 (format nil " (~A)" resourcetype)
                                 ""))))
    (cl-who:with-html-output
      (outstr nil
              :prologue t
              :indent t)
      (:html
        (:head
          (:title (princ title-string outstr))
          ;; Stylesheets
          (loop for sheet in (append '("common") stylesheets)
                do (cl-who:htm (:link :rel "stylesheet"
                                      :type "text/css"
                                      :href (format nil "/static/css/~A.css" sheet))))
          ;; Javascript
          (loop for script in (append '("jquery-3.6.0" "common") javascripts)
                do (cl-who:htm (:script :src (format nil "/static/js/~A.js" script)))))
        (:body
          (:div
            :class "default-grid"
            (:h1 :id "title" (princ title-string outstr))
            (navbar outstr navbar-items
                    :path path
                    :authorised-p authorised-p)
            ;; Body-content
            (princ content outstr)
            ;; Dependent resources
            (when (or dependent-resources authorised-p)
              (cl-who:htm
                (:div :id "dependents"
                      (:h2 :id "dependents_header" :class "page-bottom-title" "Dependent resources")
                      (:ul :id "dependents_list" :class "page-bottom-list"
                           (loop for dependent in dependent-resources
                                 do (cl-who:htm (:li (:a :href (format nil "/display~{/~A~}/~A/~A/~A"
                                                                       path
                                                                       (relationship dependent)
                                                                       (target-type dependent)
                                                                       (uid dependent))
                                                         (format outstr "~A/~A/~A"
                                                                 (relationship dependent)
                                                                 (target-type dependent)
                                                                 (uid dependent))))))))))
            ;; Add-dependent-resource link
            (when authorised-p
              (cl-who:htm
                (:div :id "add-dependent"
                      (:a :id "add-dependent-link" :href (format nil "/create-dependent~{/~A~}" path)
                          "Add a dependent resource"))))
            ;; Outbound links
            (when (or outbound-links authorised-p)
              (cl-who:htm
                (:div :id "outbound-links"
                      (:h2 :id "outbound_links" :class "page-bottom-title" "Outbound links")
                      (:ul :id "outbound_linklist" :class "page-bottom-list"
                           (loop for link in outbound-links
                                 do (cl-who:htm (:li (:a :href (format nil "/display~A" (canonical-path link))
                                                         (format outstr "~A: ~A"
                                                                 (relationship link)
                                                                 (canonical-path link))))))))))
            ;; Edit-tags link
            (when authorised-p
              (cl-who:htm
                (:div :id "edit-tags"
                      (:a :id "edit-tags" :href (format nil "/edit_links~{/~A~}" path)
                          "Edit links"))))
            ;; Display comments
            (when authorised-p
              (cl-who:htm
                (:div :id "comments"
                      (:h2 "Comments:")
                      (:ul
                        (mapcar #'(lambda (comment)
                                    (cl-who:htm (:li (format outstr "~A: " (format-timestamp (createddate comment)))
                                                     (3bmd:parse-string-and-print-to-stream (text comment) outstr))))
                                (sort comments
                                      #'<
                                      :key #'createddate)))
                      ;; Comment submission form.
                      ;; NB: This POST method needs to be handled for each dispatcher.
                      (:form :method "post"
                             :action "/comments"
                             (:div#comment-submit
                               (:input :type "hidden"
                                       :name "path"
                                       :value (format nil "~{/~A~}" path))
                               (:input :type "hidden"
                                       :name "ScUID"
                                       :value uid)
                               (:textarea :name "text"
                                          :id "comment-text")
                               (:input :type "submit"
                                       :id "comment-submit"
                                       :value "Submit comment")))))))
          (:p :class "footer"
              "Powered by <a class=footer href='https://www.sysc.at/'>Syscat</a>"))))))

(defun display-default (outstr attrs)
  "Default content layout.
  attrs arg should be a list of (name . value) dotted pairs,
  where the value will be interpreted by cl-who:with-html-output."
  (cl-who:with-html-output
    (outstr)
    (cl-who:htm
      (:div :class "content-default"
            (:h2 :id "title-attributes" "Item attributes")
            (:table :id "table-attributes"
                    (mapcar #'(lambda (attr)
                                `(:tr
                                   (:td :class "attrnames" ,(car attr))
                                   (:td :class "attrvals" ,(cdr attr))))
                            attrs))))))
