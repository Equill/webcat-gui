;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Display an individual resource

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defun sort-linked-resources (resources)
  "Sort the list of resources returned by `get-linked-resources`"
  (unless (every #'(lambda (obj) (typep obj 'linked-resource)) resources)
    (error "This isn't a list of linked-resource instances!"))
  (log-message :debug (format nil "Sorting resource-list ~{~A~^, ~}"
                              (mapcar #'(lambda (res)
                                          (format nil "/~A~A" (relationship res) (canonical-path res)))
                                      resources)))
  (stable-sort
    (stable-sort
      (sort resources #'string< :key #'uid)
      #'string< :key #'target-type)
    #'string< :key #'relationship))

(defun listify-outbound-links (links filter)
  "Filter a list of `linked-resource` instances, then sort them."
  (log-message :debug "Listifying outbound links.")
  (sort-linked-resources (remove-if-not filter links)))


(defgeneric display-element (outstr attrdef val)
  (:documentation "Render different attribute-types for display.
  attrdef is really only used for dispatching."))

(defmethod display-element ((outstr stream)
                            (attrdef schema-rtype-attr-varchar)
                            (val string))
  ;; attrdef is only there for dispatching
  (declare (ignore attrdef))
  (3bmd:parse-string-and-print-to-stream val outstr))

(defmethod display-element ((outstr stream)
                            (attrdef schema-rtype-attr-text)
                            (val string))
  ;; attrdef is only there for dispatching
  (declare (ignore attrdef))
  (3bmd:parse-string-and-print-to-stream val outstr))

(defmethod display-element ((outstr stream)
                            (attrdef schema-rtype-attr-boolean)
                            val)
  ;; attrdef is only there for dispatching
  (declare (ignore attrdef))
  (princ (if val "true" "false")
         outstr))

(defmethod display-element ((outstr stream)
                            (attrdef schema-rtype-attr-integer)
                            (val integer))
  ;; attrdef is only there for dispatching
  (declare (ignore attrdef))
  (princ val outstr))

;; For cases where an integer was actually stored as a string
(defmethod display-element ((outstr stream)
                            (attrdef schema-rtype-attr-integer)
                            (val string))
  ;; attrdef is only there for dispatching
  (declare (ignore attrdef))
  ;; Be graceful about parsing it as an integer; get what you can.
  (princ (parse-integer val :junk-allowed t) outstr))

(defun display-content-default (&key attrdefs
                                     content
                                     uri-parts
                                     uid
                                     sessionid)
  "Default renderer for the Content portion of a resource."
  (declare (ignore uri-parts uid sessionid))
  (cl-who:with-html-output-to-string
    (content-string nil
                    :prologue nil
                    :indent t)
    (:div
      :class "content"
      (:h2 :id "title-attributes" "Item attributes")
      (:table :id "table-attributes"
       (loop for attr in attrdefs
             do
             (let ((existing-value (gethash (name attr) content)))
               (log-message :debug
                            (format nil "Rendering content element '~A' with value '~A' of type ~A"
                                    (type-of attr) existing-value (type-of existing-value)))
               (cl-who:htm
                 (:tr
                   (:td :class "attrnames"
                    (princ (name attr) content-string))
                   (:td :class "attrvals"
                    (if existing-value
                        (display-element content-string attr existing-value)
                        ""))))))))))

(defparameter *content-renderers* (make-hash-table :test 'equal))

(defun display-item ()
  "Display an item"
  (log-message :debug (format nil "Handling display request from URI ~A" (tbnl:request-uri*)))
  ;; Remember to drop the initial dispatching element from the URI
  (let ((uri-parts (cdr (get-uri-parts (tbnl:request-uri*)))))
    ;; Was this a canonical URI?
    (if (canonical-path-p (rg-server tbnl:*acceptor*) uri-parts)
        ;; It's canonical. As you were...
        (let* ((resourcetype (car (last uri-parts 2)))
               (uid (car (last uri-parts)))
               (sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                            (tbnl:cookie-in "sessionid")))
               ;; Get a hash-table of attribute definitions
               (attrdefs (get-attrs (schema tbnl:*acceptor*) resourcetype))
               (content (first (rg-request-json (rg-server tbnl:*acceptor*)
                                                (format nil "~{/~A~}" uri-parts)
                                                :sessionid sessionid
                                                :parameters
                                                `(("SCattributes"
                                                   . ,(format nil "~{~A~^,~}" (mapcar #'name attrdefs))))))))
          (log-message :debug (format nil "Content: ~A" content))
          (log-message :debug (format nil "Resource-type attributes: ~A" attrdefs))
          (cond
            (content
              ;; Selectively pre-render attribute values, according to their type.
              ;; Only used as the default option, if we didn't find a special-case for this resource-type
              (let ((outbound-links (get-linked-resources
                                      (rg-server tbnl:*acceptor*)
                                      (schema tbnl:*acceptor*)
                                      uri-parts
                                      :sessionid sessionid)))
                (log-message :debug "Rendering the page")
                (setf (tbnl:content-type*) "text/html")
                (setf (tbnl:return-code*) tbnl:+http-ok+)
                (with-output-to-string (outstr)
                  (detailed-clwho-layout
                    outstr
                    ;; If it's a wikipage _and_ it has a title, use that.
                    ;; Otherwise, just de-url-escape the UID
                    :title (or (gethash "title" content)
                               (uid-to-title uid))
                    ;; Is the client authorised to make changes?
                    :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                            (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                                 sessionid))
                                    t)
                    :navbar-items (navbar-items tbnl:*acceptor*)
                    :javascripts '("display")
                    :path uri-parts
                    ;; Additional stylesheets for specific resource-types
                    :stylesheets (append
                                   '("display")
                                   (when (equal "Files" resourcetype)
                                     '("files_display"))
                                   (when (equal "Tasks" resourcetype)
                                     '("tasks_display")))
                    ;; Render the content.
                    ;; If we've defined a renderer in *content-renderers*, use that.
                    ;; Otherwise, fall back to the default renderer.
                    :content (apply (gethash resourcetype *content-renderers*
                                             #'display-content-default)
                                    (list :content content
                                          :attrdefs attrdefs
                                          :uri-parts uri-parts
                                          :uid uid
                                          :sessionid sessionid))
                    :outbound-links (listify-outbound-links
                                      outbound-links
                                      #'(lambda (link) (not (dependent-p link))))
                    :dependent-resources (listify-outbound-links
                                           outbound-links
                                           (lambda (link)
                                             (and (dependent-p link)
                                                  (not (and (equal resourcetype "CornellNotes")
                                                            (equal (relationship link) "INCORPORATES_SUBNOTE"))))))
                    :comments (get-comments (rg-server tbnl:*acceptor*)
                                            ;; Reassemble the request URI now that it's stripped of
                                            ;; URI query parameters.
                                            (format nil "~{/~A~}" uri-parts)
                                            :sessionid sessionid))
                  :stream outstr)))
            ;; No such resource, but we do have a resourcetype and UID.
            ;; Redirect to the item-creation page.
            ((and resourcetype
                  uid)
             (tbnl:redirect (format nil "/create?resourcetype=~A&ScUID=~A" resourcetype uid)))
            ;; No content, and not enough for constructing a new item: respond with a blank look.
            (t
              (progn
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-not-found+)
                "No content"))))
        ;; URI was not canonical
        (progn
          (log-message :warn "This wasn't a canonical URI")
          (setf (tbnl:content-type*) "text/plain")
          (setf (tbnl:return-code*) tbnl:+http-not-found+)
          "This was not a canonical URI. Please try again with a real one."))))


;;; Define resourcetype-specific renderers

(setf (gethash "Tasks" *content-renderers*)
      (lambda (&key attrdefs content uri-parts uid sessionid)
        (declare (ignore attrdefs uri-parts uid sessionid))
        (cl-who:with-html-output-to-string
          (content-string nil
                          :prologue nil
                          :indent t)
          (:div :class "task-grid"
           ;; State
           (:div :id "current-state-name" :class "taskattr_name" "Current state:")
           (:div :id "current-state-value"
            :class "taskattr_value"
            (princ (or (gethash "currentstate" content) "(No current-state found)")
                   content-string))
           ;; Next actions
           (:div :id "next-actions-name" :class "taskattr_name" "Next actions:")
           (:div :id "next-actions-value"
            :class "taskattr_value"
            (princ (or (gethash "nextactions" content) "(No next actions found)")
                   content-string))
           ;; Description
           (:div :id "task_description"
                 (if (gethash "description" content)
                   (3bmd:parse-string-and-print-to-stream (gethash "description" content) content-string)
                   (princ "(No description found)" content-string)))
           ;; Importance
           (:div :id "importance-name" :class "taskattr_name" "Importance:")
           (:div :id "importance-value"
            :class "taskattr_value"
            (princ (or (gethash "importance" content) "(No importance found)")
                   content-string))
           ;; Urgency
           (:div :id "urgency-name" :class "taskattr_name" "Urgency:")
           (:div :id "urgency-value"
            :class "taskattr_value"
            (princ (or (gethash "urgency" content) "(No importance found)")
                   content-string))
           ;; Scale
           (:div :id "scale-name" :class "taskattr_name" "Scale:")
           (:div :id "scale-value"
            :class "taskattr_value"
            (princ (or (gethash "scale" content) "(No scale found)")
                   content-string))
           ;; Status
           (:div :id "status-name" :class "taskattr_name" "Status:")
           (:div :id "status-value"
            :class "taskattr_value"
            (princ (or (gethash "status" content) "(No status found)")
                   content-string))))))

(setf (gethash "CornellNotes" *content-renderers*)
      (lambda (&key attrdefs
                    content
                    uri-parts
                    uid
                    sessionid)
        (declare (ignore attrdefs uid))
        (let ((subnotes
                (mapcar #'(lambda (obj)
                            (make-instance 'cornell-subnote
                                           :uid (gethash "ScUID" obj)
                                           :createddate (gethash "ScCreateddate" obj)
                                           :title (or (gethash "title" obj)
                                                      (uid-to-title (gethash "ScUID" obj)))
                                           :explanatory-text (gethash "explanatorytext" obj)))
                        (sort
                          (rg-request-json
                            (rg-server tbnl:*acceptor*)
                            (format nil "~{/~A~}/INCORPORATES_SUBNOTE/CornellSubNotes"
                                    uri-parts)
                            :sessionid sessionid
                            :parameters `(("SCattributes" . "title,explanatorytext")))
                          #'string<
                          :key #'(lambda (entry)
                                   (cdr (gethash "ScUID" entry)))))))
          (log-message :debug (format nil "Rendering with subnotes ~A" subnotes))
          (cl-who:with-html-output-to-string
            (content-string nil
                            :prologue nil
                            :indent t)
            (:div
              :class "cornell-notes"
              (:table
                :class "cornell-subnotes"
                (mapcar #'(lambda (subnote)
                            (cl-who:htm
                              (:tr
                                (:td
                                  :class "cornell-keyword"
                                  (:a :href (format nil "/display~{/~A~}/INCORPORATES_SUBNOTE/CornellSubNotes/~A"
                                                    uri-parts
                                                    (uid subnote))
                                   (princ (if (title subnote)
                                              (title subnote)
                                              (uid-to-title (uid subnote)))
                                          content-string)))
                                (:td (princ (explanatory-text subnote) content-string)))))
                        subnotes))
              (:hr)
              (:div :class "content"
               (if (gethash "summary" content)
                   (3bmd:parse-string-and-print-to-stream
                    (gethash "summary" content)
                    content-string)
                   "")))))))

(setf (gethash "Wikipages" *content-renderers*)
      (lambda (&key attrdefs
                    content
                    uri-parts
                    uid
                    sessionid)
        (declare (ignore attrdefs uri-parts uid sessionid))
        (cl-who:with-html-output-to-string
          (content-string nil
                          :prologue nil
                          :indent t)
          (:div :class "content"
           (if (gethash "text" content)
               (3bmd:parse-string-and-print-to-stream (gethash "text" content) content-string)
               "")))))

(setf (gethash "Files" *content-renderers*)
      (lambda (&key attrdefs
                    content
                    uri-parts
                    uid
                    sessionid)
        (declare (ignore attrdefs))
        (let ((mediatype
                (gethash "name"
                         (first
                           (rg-request-json
                             (rg-server tbnl:*acceptor*)
                             (format nil "~{/~A~}/HAS_MEDIA_TYPE/MediaTypes" uri-parts)
                             :sessionid sessionid
                             :parameters '(("SCattributes" . "name")))))))
          (cl-who:with-html-output-to-string
            (content-string nil
                            :prologue nil
                            :indent t)
            (:div :class "task-grid"
             ;; Title
             (:h1 (princ (gethash "title" content) content-string))
             ;; File and link
             (:div :id "files_image"
              (:a :href (format nil "/files/v1/~A" uid)
               (:img :id "files_image"
                :src (format nil "/files/v1/~A" uid))))
             ;; MIME-type
             (:div :id "mimetype-name"
              :class "fileattr_name"
              "MIME-type")
             (:div :id "mimetype-value"
              :class "fileattr_value"
              (princ mediatype content-string))
             ;; Notes
             (:div :id "notes"
              :class "notes"
              (if (gethash "notes" content)
                  (3bmd:parse-string-and-print-to-stream (gethash "notes" content) content-string)
                  "")))))))
