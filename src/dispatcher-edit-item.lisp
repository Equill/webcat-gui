;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Edit an individual resource

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defgeneric generate-edit-element (outstr attrdef &optional existing-value)
  (:documentation "Generate an edit-field, text-area or checkbox definition, according to the class of attrdef.
                   If existing-value is specified, pre-populate the field with it."))

(defmethod generate-edit-element ((outstr stream)
                                  (attrdef schema-rtype-attr-varchar)
                                  &optional (existing-value ""))
  (cl-who:with-html-output
    (outstr)
    (if (attrvalues attrdef)
        ;; If it has values to select from, create a drop-down
        (cl-who:htm
          (:select :class "attrvals"
           :name (name attrdef)
           (loop for val in (attrvalues attrdef)
                 do (if (equal existing-value val)
                        (cl-who:htm
                          (:option :name val
                           :selected "true"
                           (princ val outstr)))
                        (cl-who:htm
                          (:option :name val
                           (princ val outstr)))))))
        ;; If it does *not* have values, create an input field
        (cl-who:htm
          (:input :type "text"
           :class "attrvals"
           :name (name attrdef)
           :value existing-value)))))

(defmethod generate-edit-element ((outstr stream)
                                  (attrdef schema-rtype-attr-text)
                                  &optional (existing-value ""))
  (cl-who:with-html-output
    (outstr)
    (cl-who:htm (:textarea :class "attrvals"
                 :name (name attrdef)
                 (princ existing-value outstr)))))

(defmethod generate-edit-element ((outstr stream)
                                  (attrdef schema-rtype-attr-boolean)
                                  &optional existing-value)
  (cl-who:with-html-output
    (outstr)
    (cl-who:htm (:input :type "checkbox"
                 :class "attrvals"
                 :name (name attrdef)
                 :checked existing-value))))

(defmethod generate-edit-element ((outstr stream)
                                  (attrdef schema-rtype-attr-integer)
                                  &optional existing-value)
  ;; Assemble this in steps, to keep the code understandable.
  ;; Put together the stuff that'll always be here:
  (let ((data `(:input :type "number"
                       :class "attrvals"
                       :name ,(name attrdef))))
    ;; If a minimum and/or maximum is specified in the resourcetype, add them
    (when (minimum attrdef)
      (setf data (append data `(:min ,(minimum attrdef)))))
    (when (maximum attrdef)
      (setf data (append data `(:max ,(maximum attrdef)))))
    ;; If we got an existing value, add that last
    (when existing-value
      (setf data (append data (list existing-value))))
    (log-message :debug (format nil "Rendering number field with data: ~A" data))
    ;; Now render the result
    (cl-who:with-html-output
      (outstr)
      (:input :type "number"
              :class "attrvals"
              :name (name attrdef)
              :pattern "[0-9]"
              :value existing-value))))


(defun edit-resource ()
  "Handle the edit-page for an item"
  (log-message :debug (format nil "Attempting to edit an item with URI ~A" (tbnl:request-uri*)))
  (let ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid")))
        ;; Remember to drop the initial dispatching element from the URI
        (uri-parts (cdr (get-uri-parts (tbnl:request-uri*)))))
    (cond
      ;; Client isn't authorised; bounce them straight back to the display page.
      ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
           (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                (not sessionid)))
       (log-message :debug (format nil "Edit request not authorised; redirecting client to ~A"
                                   (tbnl:request-uri*)))
       (tbnl:redirect (format nil "/display~{/~A~}" uri-parts)))
      ;; Display the edit-resource form
      ((equal (tbnl:request-method*) :GET)
       (let* ((resourcetype (car (last uri-parts 2)))
              (uid (car (last uri-parts)))
              ;; Get a hash-table of attribute definitions
              (attrdefs (get-attrs (schema tbnl:*acceptor*) resourcetype))
              (content (first (rg-request-json (rg-server tbnl:*acceptor*)
                                               (format nil "~{/~A~}" uri-parts)
                                               :sessionid sessionid
                                               :parameters
                                               `(("SCattributes"
                                                  . ,(format nil "~{~A~^,~}" (mapcar #'name attrdefs))))))))
         (log-message :debug (format nil "Attempting to display the edit page for ~{/~A~}" uri-parts))
         (log-message :debug (format nil "Resourcetype: ~A" resourcetype))
         (log-message :debug (format nil "UID: ~A" uid))
         (log-message :debug (format nil "Content: ~A" content))
         ;; Render the content according to resourcetype
         (if content
           (cond ((equal resourcetype "Wikipages")
                  (let ((title (or (gethash "title" content) (uid-to-title uid))))
                    (log-message :debug (format nil "Rendering wikipage ~A" uid))
                    (setf (tbnl:content-type*) "text/html")
                    (setf (tbnl:return-code*) tbnl:+http-ok+)
                    (with-output-to-string (outstr)
                      (simple-clwho-layout
                        outstr
                        :title title
                        :stylesheets '("edit" "display")
                        :path uri-parts
                        ;; Is the client authorised to make changes?
                        :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                                (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                                     sessionid))
                                        t)
                        :navbar-items (navbar-items tbnl:*acceptor*)
                        :content
                        (cl-who:with-html-output-to-string
                          (content-string nil
                                          :prologue nil
                                          :indent t)
                          (:form :class "formgrid-wikipage"
                                 :action (format nil "/editresource/Wikipages/~A" uid)
                                 :method "post"
                                 ;; Title
                                 (:div :class "wikititle-title" "Title:")
                                 (:input :class "wikititle-value"
                                         :name "title"
                                         :value title)
                                 ;; Content
                                 (:textarea :id "wikipage-content"
                                            :name "text"
                                            :cols 100
                                            :rows 40
                                            (princ (or (gethash "text" content) "") content-string))
                                 ;; Version comment
                                 (:div :id "versioncomment"
                                       "Version comment: "
                                       (:input :name "ScVersioncomment"
                                               :id "versioncomment"))
                                 ;; Submit button
                                 (:div :id "submit-row"
                                       (:input :id "wikipage-submit"
                                               :type "submit"
                                               :value "Update"))))))))
                 (t
                   (progn
                     (log-message :debug (format nil "Rendering ~{/~A~}" uri-parts))
                     ;; Render the attributes for editing
                     (let ((attributes-to-display (get-attrs (schema tbnl:*acceptor*) resourcetype)))
                       (log-message :debug (format nil "Attributes: ~A" attributes-to-display))
                       (setf (tbnl:content-type*) "text/html")
                       (setf (tbnl:return-code*) tbnl:+http-ok+)
                       (with-output-to-string (outstr)
                         (simple-clwho-layout
                           outstr
                           :title (format nil "Edit ~A: ~A" resourcetype (uid-to-title uid))
                           :path uri-parts
                           :stylesheets '("edit" "display")
                           ;; Is the client authorised to make changes?
                           :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                                   (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                                        sessionid))
                                           t)
                           :navbar-items (navbar-items tbnl:*acceptor*)
                           :content
                           (cl-who:with-html-output-to-string
                             (content-string nil
                                             :prologue nil
                                             :indent t)
                             (:form :class "formgrid-edititem"
                                    :action (format nil "/editresource~{/~A~}" uri-parts)
                                    :method "post"
                                    (:h2 :id "title-attributes" "Edit attributes")
                                    (:table :class "attributes"
                                            (loop for attr in attributes-to-display
                                                  do (let ((existing-value (gethash (name attr) content)))
                                                       (log-message :debug (format nil "Rendering attribute ~A" (name attr)))
                                                       (cl-who:htm
                                                         (:tr
                                                           (:td (:p (princ (name attr) content-string)))
                                                           (:td
                                                             (if existing-value
                                                               (generate-edit-element content-string attr existing-value)
                                                               (generate-edit-element content-string attr))))))))
                                    ;; Version comment
                                    (:div :id "versioncomment"
                                          "Version comment: "
                                          (:input :name "ScVersioncomment"
                                                  :id "versioncomment"))
                                    (:div :id "submit-row"
                                          (:input :id "wikipage-submit"
                                                  :type "submit"
                                                  :value "Update"))))))))))
           ;; Content not retrieved
           (progn
             (setf (tbnl:content-type*) "text/plain")
             (setf (tbnl:return-code*) tbnl:+http-not-found+)
             "No content"))))
      ;;; POST request: update the resource
      ((equal (tbnl:request-method*) :POST)
       ;; Remember to drop the initial dispatching element from the URI
       (let* ((resourcetype (car (last uri-parts 2)))
              (uid (car (last uri-parts)))
              ;; Extract attributes relevant to this resourcetype
              (valid-attrnames
                (append
                  (mapcar #'name
                          (get-attrs (schema tbnl:*acceptor*) resourcetype))
                  (list "ScVersioncomment")))
              (validated-attrs
                (mapcar #'(lambda (param)
                            (log-message :debug (format nil "Validating parameter '~A' with value '~A'"
                                                        (car param) (cdr param)))
                            (when (member (car param)
                                          valid-attrnames
                                          :test #'equal)
                              param))
                        (tbnl:post-parameters*))))
         (log-message :debug (format nil "Processing edit request for ~A ~A" resourcetype uid))
         (log-message :debug (format nil "Validated attributes: ~A" validated-attrs))
         ;; Send the update
         (multiple-value-bind (body status-code)
           (rg-post-json (rg-server tbnl:*acceptor*)
                         (format nil "~{/~A~}" uri-parts)
                         :payload validated-attrs
                         :put-p t
                         :sessionid sessionid)
           ;; Did it work?
           (if (and (> status-code 199)
                    (< status-code 300))
             ;; Happy path
             (tbnl:redirect (format nil "/display~{/~A~}" uri-parts))
             ;; Less-happy path
             (with-output-to-string (outstr)
               (setf (tbnl:content-type*) "text/html")
               (setf (tbnl:return-code*) tbnl:+http-bad-request+)
               (simple-clwho-layout
                 outstr
                 :title (format nil "Failed to update ~{/~A~}" uri-parts)
                 :path uri-parts
                 ;; Is the client authorised to make changes?
                 :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                         (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                              sessionid))
                                 t)
                 :stylesheets '("display")
                 :javascripts '("display")
                 :content (with-output-to-string (content-string)
                            (display-default
                              content-string
                              `(("Server message" . ,body))))))))))
      ;; Fallback: not by this method
      (t (method-not-allowed)))))
