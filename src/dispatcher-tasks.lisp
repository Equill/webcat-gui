;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Custom view for Tasks

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun tasks ()
  "Display the tasks page"
  (cond
    ((equal (tbnl:request-method*) :GET)
     (let* ((sessionid (tbnl:cookie-in "sessionid"))
            (task-attrs (get-attrs (schema tbnl:*acceptor*) "Tasks"))
            (statuses-requested (filter-params "status" (tbnl:get-parameters*)))
            (scale-requested (filter-params "scale" (tbnl:get-parameters*)))
            (urgency-requested (filter-params "urgency" (tbnl:get-parameters*)))
            (importance-requested (filter-params "importance" (tbnl:get-parameters*)))
            (tbnl-formatted-results
              (search-for-tasks (webcat-gui::rg-server webcat-gui::*webcat-gui-acceptor*)
                                :statuses statuses-requested
                                :scale scale-requested
                                :urgency urgency-requested
                                :importance importance-requested
                                :uid-regex (tbnl:get-parameter "uid_regex")
                                :sessionid sessionid)))
       ;; Debug logging for what we've obtained so far
       (log-message :debug (format nil "Attributes: ~A" task-attrs))
       (log-message :debug (format nil "Statuses available: ~A" (get-enum-vals "status" task-attrs)))
       (log-message :debug (format nil "tbnl-formatted search results: ~A" tbnl-formatted-results))
       (setf (tbnl:content-type*) "text/html")
       (setf (tbnl:return-code*) tbnl:+http-ok+)
       (with-output-to-string (outstr)
         (simple-clwho-layout
           outstr
           :title "Webcat tasks search"
           :stylesheets '("tasks_search")
           :javascripts '("search")
           ;; Is the client authorised to make changes?
           :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                   (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                        sessionid))
                           t)
           :navbar-items (navbar-items tbnl:*acceptor*)
           :content
           (cl-who:with-html-output-to-string
             (content-string nil
                             :prologue nil
                             :indent t)
             (:div :id "searchbox"
                   (:form :action "/Tasks"
                          :method "get"
                          :class "formgrid-tasks"
                          :id "searchform"
                          ;; Other criteria
                          (:div :id "refine-title" "Refine your search")
                          (:div :id "uid-regex-name" :class "criteria-name" "UID regex")
                          (:div :id "uid-regex-value"
                                :class "criteria-value"
                                (:input :type "text"
                                        :name "uid_regex"
                                        :value (or (tbnl:get-parameter "uid_regex") "")))
                          ;; Importance
                          (:div :id "importance-name" :class "criteria-name" "Importance")
                          (:select :id "importance-value"
                                   :name "importance"
                                   :class "criteria-value"
                                   :multiple "multiple"
                                   :size 3
                                   (:option :value "" "Any")
                                   (loop for imp in (get-enum-vals "importance" task-attrs)
                                         do (if (equal imp (tbnl:get-parameter "importance"))
                                              (cl-who:htm (:option :value imp
                                                                   :selected "selected"
                                                                   (princ imp content-string)))
                                              (cl-who:htm (:option :value imp
                                                                   (princ imp content-string))))))
                          ;; Urgency
                          (:div :id "urgency-name" :class "criteria-name" "Urgency")
                          (:select :id "urgency-value"
                                   :name "urgency"
                                   :class "criteria-value"
                                   :multiple "multiple"
                                   :size 3
                                   (:option :value "" "Any")
                                   (loop for urge in (get-enum-vals "urgency" task-attrs)
                                         do (if (equal urge (tbnl:get-parameter "urgency"))
                                              (cl-who:htm (:option :value urge
                                                                   :selected "selected"
                                                                   (princ urge content-string)))
                                              (cl-who:htm (:option :value urge
                                                                   (princ urge content-string))))))
                          ;; Scale
                          (:div :id "scale-name" :class "criteria-name" "Scale")
                          (:select :id "scale-value"
                                   :name "scale"
                                   :class "criteria-value"
                                   :multiple "multiple"
                                   :size 3
                                   (:option :value "" "Any")
                                   (loop for scale in (get-enum-vals "scale" task-attrs)
                                         do (if (equal scale (tbnl:get-parameter "scale"))
                                              (cl-who:htm (:option :value scale
                                                                   :selected "selected"
                                                                   (princ scale content-string)))
                                              (cl-who:htm (:option :value scale
                                                                   (princ scale content-string))))))
                          ;; Status
                          (:div :id "status-name" :class "criteria-name" "Status")
                          (:select :id "status-value"
                                   :name "status"
                                   :class "criteria-value"
                                   :multiple "multiple"
                                   :size 3
                                   (:option :value "" "Any")
                                   (loop for status in (get-enum-vals "status" task-attrs)
                                         do (if (equal status (tbnl:get-parameter "status"))
                                              (cl-who:htm (:option :value status
                                                                   :selected "selected"
                                                                   (princ status content-string)))
                                              (cl-who:htm (:option :value status
                                                                   (princ status content-string))))))
                          ;; Submit button
                          (:div :id "submit-row"
                                (:input :type "submit" :value "Search" :id "searchbutton")))
                   ;; Search results
                   (:div :id "searchresults"
                         (:h2 "Search results")
                         (:table :class "results-list"
                                 (:tr
                                   (:th "Status")
                                   (:th "Urgency")
                                   (:th "Importance")
                                   (:th "Scale")
                                   (:th "Title")
                                   (:th "Deadline"))
                                 (loop for result in tbnl-formatted-results
                                       do (cl-who:htm
                                            (:tr
                                              (:td :class "results-list" (princ (getf result :status) content-string))
                                              (:td :class "results-list" (princ (getf result :urgency) content-string))
                                              (:td :class "results-list" (princ (getf result :importance) content-string))
                                              (:td :class "results-list" (princ (getf result :scale) content-string))
                                              (:td :class "results-list"
                                                   (:a :href (format nil "display/Tasks/~A" (getf result :uid))
                                                       (princ (getf result :title) content-string)))
                                              (:td :class "results-list"
                                                   (princ (or (getf result :deadline) "") content-string)))))))))))))
    ;; Fallback: not by this method
    (t (method-not-allowed))))
