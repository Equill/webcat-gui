;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:webcat-gui)

(defclass webcat-gui-acceptor (tbnl:easy-acceptor)
  ;; Subclass attributes
  ((cookie-domain :initarg :cookie-domain
                  :reader cookie-domain
                  :type string
                  :documentation "The domain for which we're setting cookies, which may or may not coincide with the listen-address.")
   (rg-server :initarg :rg-server
              :reader rg-server
              :type rg-server
              :initform (error ":rg-server is a required parameter.")
              :documentation "Object containing the details needed to connect to the backend Syscat server.")
   (static-path :initarg :static-path
                :reader static-path
                :type string
                :documentation "Filesystem path to the directory containing the static assets, e.g. stylesheets and Javascript files.")
   (schema :initarg :schema
           :accessor schema
           :type hash-table
           :documentation "Cached representation of the Syscat server's schema.")
   (auth-policy :initarg :auth-policy
                :reader auth-policy
                :type string
                :initform "open"
                :documentation "The authentication and authorisation policy of the Syscat server. Defaults to 'open'.")
   (navbar-items :initarg :navbar-items
                 :accessor navbar-items
                 :type (or null navbar-items)
                 :initform nil
                 :documentation "The object carrying information about what to put in the nav-bar."))
  ;; Superclass defaults
  (:default-initargs :address "127.0.0.1")
  ;; Note to those asking.
  (:documentation "vhost object, subclassed from tbnl:easy-acceptor. Carries information about the datastore being used, among other things."))

(defun make-defined-rg-server ()
  "Make an rg-server instance according to the available configs."
  (make-rg-server
    ;; This is the name used in the Host: header.
    ;; If it's not set explicitly, fall back to using the address,
    ;; and if *that* isn't set, fall back to the same default.
    :hostname (or (sb-ext:posix-getenv "SC_HOSTNAME")
                  (sb-ext:posix-getenv "SC_ADDRESS")
                  "localhost")
    :address (or (sb-ext:posix-getenv "SC_ADDRESS")
                 "localhost")
    :port (or (when (sb-ext:posix-getenv "SC_PORT")
                (parse-integer (sb-ext:posix-getenv "SC_PORT")))
              4950)))

(defun make-acceptor (&key rg-server)
  "Return an instance of 'webcat-gui-acceptor, a subclass of tbnl:easy-acceptor."
  (declare (type (or null rg-server) rg-server))
  (make-instance 'webcat-gui-acceptor
                 :cookie-domain (or (sb-ext:posix-getenv "COOKIE_DOMAIN")
                                    (sb-ext:posix-getenv "LISTEN_ADDR")
                                    "localhost")
                 :address (or (sb-ext:posix-getenv "LISTEN_ADDR")
                              "localhost")
                 :port (or (when (sb-ext:posix-getenv "LISTEN_PORT")
                             (parse-integer (sb-ext:posix-getenv "LISTEN_PORT")))
                           8080)
                 :static-path (make-pathname :defaults
                                             (or (sb-ext:posix-getenv "STATIC_PATH")
                                                 (format nil "~Adevel/syscat/webcat-gui/src/static/"
                                                         (user-homedir-pathname))))
                 :schema (fetch-schema rg-server)
                 :auth-policy (if (member (sb-ext:posix-getenv "SC_AUTH_POLICY")
                                          '("open"
                                            "read-only"
                                            "authenticated")
                                          :test #'equal)
                                (sb-ext:posix-getenv "SC_AUTH_POLICY")
                                "open")
                 ;; Send all logs to STDOUT, and let Docker sort 'em out
                 :access-log-destination (make-synonym-stream 'cl:*standard-output*)
                 :message-log-destination (make-synonym-stream 'cl:*standard-output*)
                 ;; Syscat connection details
                 :rg-server (or rg-server (make-defined-rg-server))))

(defstruct rg-server
  "The details needed to connect to the backend Syscat server."
  ;; Value to use for the Host: header, which may differ from the address
  (hostname "localhost" :type string :read-only t)
  ;; The actual address to connect to
  (address "localhost" :type string :read-only t)
  ;; The port on which the server is listening
  (port 4950 :type integer :read-only t)
  ;; API endpoint URIs.
  ;; Encoded here on the assumption that it's safer to fetch them from this object on each
  ;; request than type them correctly everywhere they're used.
  (raw-base "/raw/v1" :type string :read-only t)
  (files-base "/files/v1" :type string :read-only t)
  (schema-base "/schema/v1" :type string :read-only t)
  (auth-base "/auth/v1" :type string :read-only t))


(defclass schema-rtype ()
  ((name :initarg :name
         :reader name
         :type string
         :initform (error "name is mandatory"))
   (dependent :initarg :dependent
              :reader dependent
              :type bool
              :initform nil)
   (description :initarg :description
                :reader description
                :type string
                :initform "")
   (attributes :initarg :attributes
               :reader attributes
               :type list
               :initform nil
               :documentation "A list of schema-rtype-attrs objects.")
   (relationships :initarg :relationships
                  :reader relationships
                  :type list
                  :initform nil
                  :documentation "A list of outbound-rels instances."))
  (:documentation "A resourcetype, as returned by a call to Syscat's schema API."))

(defclass schema-rtype-attrs ()
  ((name :initarg :name
         :reader name
         :type string
         :initform (error "name is mandatory"))
   (description :initarg :description
                :reader description
                :type (or null string)
                :initform nil)
   (readonly :initarg :readonly
           :reader readonly
           :type boolean
           :initform nil))
  (:documentation "Attributes of resource-types. Abstract type: *do not* instantiate directly; instead, use one of its subclasses."))

(defclass schema-rtype-attr-varchar (schema-rtype-attrs)
  ((maxlength :initarg :maxlength
              :reader maxlength
              :type (or null integer)
              :initform nil)
   (attrvalues :initarg :attrvalues
               :reader attrvalues
               :type (or null list)
               :initform nil))
  (:documentation "Variable-length string, for one-line values with optionally limited length. For bulk text, use schema-rtype-attr-text."))

(defclass schema-rtype-attr-text (schema-rtype-attrs)
  ()
  (:documentation "Block text, up to 65535 characters. For one-liners, see schema-rtype-attr-varchar."))

(defclass schema-rtype-attr-integer (schema-rtype-attrs)
  ((minimum :initarg :minimum
            :reader minimum
            :type (or null integer)
            :initform nil)
   (maximum :initarg :maximum
            :reader maximum
            :type (or null integer)
            :initform nil))
  (:documentation "Integer, with optional minimum and maximum values."))

(defclass schema-rtype-attr-boolean (schema-rtype-attrs)
  ()
  (:documentation "Boolean. Does what you'd expect."))


(defun make-schema-rtype-attrs (attribute)
  "Instantiate a subclass of schema-rtype-attrs according to the data provided.
  `attribute` is expected to be an alist, as fetched from the Syscat schema endpoint."
  (declare (type hash-table attribute))
  ;; Save a bunch of repeated typing
  (let ((attrtype (gethash "type" attribute))
        (attrname (gethash "name" attribute))
        (attrdesc (gethash "description" attribute))
        (readonly (gethash "readonly" attribute)))
    (cond
      ;; Text-block
      ((equal "text" attrtype)
       (make-instance 'schema-rtype-attr-text
                      :name attrname
                      :description attrdesc
                      :readonly readonly))
      ;; Integer
      ((equal "integer" attrtype)
       (make-instance 'schema-rtype-attr-integer
                      :name attrname
                      :description attrdesc
                      :readonly readonly
                      :minimum (gethash "minimum" attribute)
                      :maximum (gethash "maximum" attribute)))
      ;; Boolean
      ((equal "boolean" attrtype)
       (make-instance 'schema-rtype-attr-boolean
                      :name attrname
                      :description attrdesc
                      :readonly readonly))
      ;; Explicitly defined as a varchar
      ((equal "varchar" attrtype)
       (make-instance 'schema-rtype-attr-varchar
                      :name attrname
                      :description attrdesc
                      :readonly readonly
                      :maxlength (gethash "maxlength" attribute)
                      :attrvalues (gethash "values" attribute)))
      ;; Default to varchar, but without maxlength or attrvalues.
      (t
        (make-instance 'schema-rtype-attr-varchar
                       :name attrname
                       :description attrdesc
                       :readonly readonly)))))


(defclass outbound-rels ()
  ((name :initarg :name
         :reader name
         :type string
         :initform (error "Required argument.")
         :documentation "The name of this relationship itself.")
   (description :initarg :description
                :reader description
                :type string
                :initform "")
   (reltype :initarg :reltype
            :reader reltype
            :type string
            :initform "any"
            :documentation "Should be one of 'any' or 'dependent'. Default is 'any'.")
   (cardinality :initarg :cardinality
                :reader cardinality
                :type string
                :initform "many:many")
   (target-types :initarg :target-types
                 :reader target-types
                 :type list
                 :initform (error "Required argument.")
                 :documentation "List of names of target resourcetypes."))
  (:documentation "Defines a valid outbound relationship from a given resource."))

(defclass linked-resource ()
  ((relationship :initarg :relationship
                 :reader relationship
                 :initform (error "Required argument.")
                 :documentation "The name of the outgoing relationship.")
   (target-type :initarg :target-type
                :reader target-type
                :initform (error "Required argument.")
                :documentation "The resourcetype of the relationship's target.")
   (dependent-p :initarg :dependent-p
                :reader dependent-p
                :initform nil
                :documentation "Whether the relationship is a dependent one.")
   (uid :initarg :uid
        :reader uid
        :initform (error "Required argument"))
   (canonical-path :initarg :canonical-path
                   :reader canonical-path
                   :initform nil
                   :documentation "The canonical path to this resource, independent of its relationship to the relationship source."))
  (:documentation "Description of a resource linked from another resource"))

(defclass resource ()
  ((uid :initarg :uid
        :reader uid
        :type (or string null)
        :initform (error "No valid resource lacks a UID"))
   (createddate :initarg :createddate
                :reader createddate
                :type integer
                :initform (error "No valid resource lacks a createddate")))
  (:documentation "Parent class for resources fetched from a Syscat server."))

(defclass comment (resource)
  ((text :initarg :text
         :reader text
         :type string
         :initform ""))
  (:documentation "A comment on any resource."))

(defclass cornell-subnote (resource)
  ((title :initarg :title
          :reader title
          :type (or null string)
          :initform nil)
   (explanatory-text :initarg :explanatory-text
                     :reader explanatory-text
                     :type string
                     :initform (progn
                                 (warn "CornellSubNotes shouldn't exist without explanatory text.")
                                 "")))
  (:documentation "A Cornell SubNotes instance"))
