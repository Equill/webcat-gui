;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Edit links from a resource

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun extract-path-parts (path)
  "Split a URI path into its constituent parts, suitably sanitised."
  (declare (type string path))
  (mapcar (lambda (part)
            ;; Ensure leading/trailing whitespace is removed
            (string-trim '(#\Tab #\Space) part))
          (remove-if #'(lambda (x)
                         (or (null x)
                             (equal x "")))
                     (cl-ppcre:split "/" path))))

(defun edit-links ()
  "Edit a resource's links to tags and other resources."
  (let ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid")))
        (uri-parts (cdr (get-uri-parts (tbnl:request-uri*)))))
    (cond
      ;; Client isn't authorised; bounce them straight back to the display page.
      ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
           (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                (not sessionid)))
       (log-message :debug (format nil "Edit request not authorised; redirecting client to ~A"
                                   (tbnl:request-uri*)))
       (tbnl:redirect (format nil "/display~{/~A~}" uri-parts)))
      ;; Display the edit-links page
      ((equal (tbnl:request-method*) :GET)
       (let ((resourcetype (car (last uri-parts 2)))
             (uid (car (last uri-parts)))
             (resource (format nil "~{/~A~}" uri-parts)))
         (with-output-to-string (outstr)
           (simple-clwho-layout
             outstr
             :title (format nil "Edit links for ~A ~A"
                            resourcetype (uid-to-title uid))
             :path uri-parts
             :stylesheets '("edit_links" "display")
             :javascripts '()
             ;; Is the client authorised to make changes?
             :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                     (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                          sessionid))
                             t)
             :navbar-items (navbar-items tbnl:*acceptor*)
             :content
             (cl-who:with-html-output-to-string
               (content-string nil
                               :prologue nil
                               :indent t)
               (:div :class "content-relforms"
                     ;; Form for adding a relationship
                     (:form :id "relationshipform"
                            :class "formgrid-add-relationship"
                            :method "get"
                            :action (format nil "/create_relationship")
                            (:div :id "title-add-rel"
                                  (:h3 "Link to other resources"))
                            (:div :id "rel-input"
                                  "URI of target resource:"
                                  (:input :id "resource_links"
                                          :name "target"
                                          :type "text"
                                          :value ""))
                            (:input :name "source"
                                    :type "hidden"
                                    :value resource)
                            (:input :type "submit"
                                    :class "rels-submit"
                                    :value "Select relationship"))))))))
      ;; POST request to create a link
      ((equal (tbnl:request-method*) :POST)
       (let ((uid (car (last uri-parts)))
             (sessionid (tbnl:cookie-in "sessionid"))
             ;; Error counter for the updates
             (update-errors ()))
         ;; Try to perform the updates
         (mapcar #'(lambda (param)
                     (cond
                       ;; Add a tag
                       ((equal (car param) "add-tags")
                        (log-message :debug (format nil "Adding tag ~A" (cdr param)))
                        (multiple-value-bind (body status-code)
                          (rg-post-json (rg-server tbnl:*acceptor*)
                                        (format nil "~{/~A~}/TAGS" uri-parts)
                                        :payload `(("target"
                                                    . ,(concatenate 'string "/Tags/" (cdr param))))
                                        :sessionid sessionid)
                          ;; Did it work?
                          (if (or (< status-code 200)
                                  (> status-code 299))
                            (push (list :attrname (concatenate 'string
                                                               "Failed to add tag " (cdr param))
                                        :attrval (format nil "~A: ~A" status-code body))
                                  update-errors))))
                       ;; Remove a tag
                       ((equal (car param) "remove-tags")
                        (log-message :debug (format nil "Removing tag ~A" (cdr param)))
                        (multiple-value-bind (status-code body)
                          (rg-delete (rg-server tbnl:*acceptor*)
                                     (format nil "~{/~A~}/TAGS" uri-parts)
                                     :payload (list (concatenate 'string "target=/Tags/" (cdr param)))
                                     :sessionid sessionid)
                          ;; Did it work?
                          (if (or (< status-code 200)
                                  (> status-code 299))
                            (push (list :attrname (concatenate 'string
                                                               "Failed to remove tag " (cdr param))
                                        :attrval (format nil "~A: ~A" status-code body))
                                  update-errors))))
                       ;; FIXME: turn this into a single-target link to /create_relationship
                       ;; Links to other resources
                       ((equal (car param) "resource_links")
                        (log-message :debug (format nil "Linking to resources '~A'" (cdr param)))
                        (mapcar #'(lambda (target)
                                    (let* ((target-parts (extract-path-parts target))
                                           (sourcepath (format nil "~{/~A~}/~A" uri-parts (first target-parts)))
                                           (targetpath (format nil "~{/~A~}" (cdr target-parts))))
                                      (log-message :debug
                                                   (format nil "Linking ~A to ~A"
                                                           sourcepath targetpath))
                                      (multiple-value-bind (body status-code)
                                        (rg-post-json (rg-server tbnl:*acceptor*)
                                                      sourcepath
                                                      :payload `(("target" . ,targetpath))
                                                      :sessionid sessionid)
                                        ;; Did it work?
                                        (when (or (< status-code 200)
                                                  (> status-code 299))
                                          ;; If not, add the error to the error-list
                                          (progn
                                            (log-message
                                              :debug
                                              (format nil "Failed to add link to '~A'. ~A: ~A"
                                                      (cdr param) status-code body))
                                            (push (list
                                                    :attrname (format nil "Failed to add link to '~A'"
                                                                      (cdr param))
                                                    :attrval (format nil "~A: ~A" status-code body))
                                                  update-errors))))))
                                ;; Split on any quantity of whitespace
                                (cl-ppcre:split "[ ]+" (cdr param))))
                       ;; Something else
                       (t (log-message :debug (format nil "Other parameter supplied to edit-tags: ~A"
                                                      param)))))
                 (tbnl:post-parameters*))
         ;; At least one of those updates broke:
         (if update-errors
           (progn
             (log-message :debug (format nil "Detected update errors: ~A" update-errors))
             (setf (tbnl:content-type*) "text/html")
             (setf (tbnl:return-code*) tbnl:+http-bad-request+)
             (with-output-to-string (outstr)
               (simple-clwho-layout
                 outstr
                 :title (format nil "Failed to create ~A" uid)
                 :path uri-parts
                 :stylesheets '("display")
                 :javascripts '("display")
                 ;; Is the client authorised to make changes?
                 :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                         (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                              sessionid))
                                 t)
                 :navbar-items (navbar-items tbnl:*acceptor*)
                 :content (display-default outstr update-errors)))))
         ;; Happy path: no errors
         (tbnl:redirect (format nil "/display~{/~A~}" uri-parts))))
      (t (method-not-allowed)))))


(defun fetch-available-relationships (schema source-resourcetype target-resourcetype)
  "Helper function for fetching the list of available relationships between two resourcetypes.
  Return a list of outbound-rels instances."
  (declare (type hash-table schema)
           (type string source-resourcetype target-resourcetype))
  (log-message :debug (format nil "Fetching available relationships from ~A to ~A"
                              source-resourcetype target-resourcetype))
  ;; Attempt to fetch the details of the source resourcetype
  (let ((source-rtype (gethash source-resourcetype schema)))
    ;; If there was one, extract the list of relationships.
    ;; If not, return nil, i.e. an empty list.
    (when source-rtype
      (remove-if-not
        #'(lambda (rel)
            (log-message :debug (format nil "Target types for resourcetype ~A: ~A"
                                        (name rel) (target-types rel)))
            (member target-resourcetype (target-types rel) :test #'equal))
        (relationships source-rtype)))))

(defun select-relationship ()
  "List the available relationships to the target resource, and provide a means of selecting one."
  (log-message :debug (format nil "Handling ~A request for URI ~A" (tbnl:request-method*) (tbnl:request-uri*)))
  (let ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid"))))
    (cond
      ;; Client isn't authorised; bounce them straight back to the display page.
      ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
           (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                (not sessionid)))
       (log-message :debug (format nil "Edit request not authorised; redirecting client to ~A"
                                   (tbnl:request-uri*)))
       (tbnl:redirect (concatenate 'string "/display" (tbnl:post-parameter "source"))))
      ;; GET request, where A source and target resource have been specified
      ((and (equal :GET (tbnl:request-method*))
            (tbnl:get-parameter "source")
            (tbnl:get-parameter "target"))
       (let* ((source-resourcetype
                (car (last (get-uri-parts (tbnl:get-parameter "source")) 2)))
              (target-resourcetype
                (car (last (get-uri-parts (tbnl:get-parameter "target")) 2)))
              ;; Get a list of alists describing the available _outbound_ relationships
              (rg-scanner (cl-ppcre:create-scanner "^SC_"))
              (available-relationships
                (remove-if (lambda (rel) (cl-ppcre:scan rg-scanner (name rel)))
                           (append
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            source-resourcetype
                                                            target-resourcetype)
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            "any"
                                                            target-resourcetype)
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            source-resourcetype
                                                            "any")
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            "any"
                                                            "any"))))
              ;; Get a list of available relationships _back_ to the source resource,
              ;; so the user can set this up in both directions in one action.
              (available-return-rels
                (remove-if (lambda (rel) (cl-ppcre:scan rg-scanner (name rel)))
                           (append
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            target-resourcetype
                                                            source-resourcetype)
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            "any"
                                                            source-resourcetype)
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            target-resourcetype
                                                            "any")
                             (fetch-available-relationships (schema tbnl:*acceptor*)
                                                            "any"
                                                            "any")))))
         (log-message :debug (format nil "Fetched list of available relationships: ~A"
                                     (mapcar #'name available-relationships)))
         (log-message :debug (format nil "Fetched list of available return relationships: ~A"
                                     (mapcar #'name available-return-rels)))
         (setf (tbnl:content-type*) "text/html")
         (setf (tbnl:return-code*) tbnl:+http-ok+)
         (with-output-to-string (outstr)
           (simple-clwho-layout
             outstr
             :title "Relationship selector"
             :path '("Select relationship")
             :stylesheets '("display" "select_relationship")
             :javascripts '()
             ;; Is the client authorised to make changes?
             :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                     (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                          sessionid))
                             t)
             :navbar-items (navbar-items tbnl:*acceptor*)
             :content
             (cl-who:with-html-output-to-string
               (content-string nil
                               :prologue nil
                               :indent t)
               (:form
                 :id "linkform"
                 :class "formgrid-select-relationship"
                 :action "/create_relationship"
                 :method "post"
                 ;; Outbound relationships
                 ;; Title
                 (:div :id "outbound-title" (:h3 "Outbound relationship:"))
                 ;; Relationship types
                 (:div :id "available-rels"
                       (:p (format content-string "~A --> ~A: "
                                   (tbnl:get-parameter "source")
                                   (tbnl:get-parameter "target")))
                       (:select :id "relationships"
                                :name "relationship"
                                :size 15
                                (loop for rel in available-relationships
                                      do (cl-who:htm (:option :value (name rel)
                                                              (format content-string
                                                                      "~A (cardinality: ~A)"
                                                                      (name rel) (cardinality rel)))))))
                 ;; Return relationships
                 ;; Title
                 (:div :id "return-title" (:h3 "Return relationship:"))
                 ;; Relationship types
                 (:div :id "available-return"
                       (:p (format content-string "~A <-- ~A: "
                                   (tbnl:get-parameter "source")
                                   (tbnl:get-parameter "target")))
                       (:select :id "return-relationships"
                                :name "return-relationship"
                                :size 15
                                (:option :value ""
                                         :selected "selected"
                                         "(no return relationship)")
                                (loop for rel in available-return-rels
                                      do (cl-who:htm (:option :value (name rel)
                                                              (format content-string
                                                                      "~A (cardinality: ~A)"
                                                                      (name rel) (cardinality rel)))))))
                 ;; Hidden fields with the information we already have
                 (:input :name "source"
                         :type "hidden"
                         :value (tbnl:get-parameter "source"))
                 (:input :name "target"
                         :type "hidden"
                         :value (tbnl:get-parameter "target"))
                 ;; Submit button
                 (:div :id "rels-submit"
                       (:input :id "linkbutton"
                               :type "submit"
                               :value "Create relationship"))))))))
      ;; GET request, where the required parameters have *not* been supplied
      ((equal :GET (tbnl:request-method*))
       (setf (tbnl:content-type*) "text/html")
       (setf (tbnl:return-code*) tbnl:+http-bad-request+)
       "Error page goes here.")
      ;; POST request, where this is actually done, and any constraint-violations are reflected.
      ((equal :POST (tbnl:request-method*))
       ;; Did we get the parameters we needed?
       (if (and (tbnl:post-parameter "source")
                (tbnl:post-parameter "target")
                (tbnl:post-parameter "relationship"))
         ;; Got what we need
         (let* ((source (tbnl:post-parameter "source"))
                (target (tbnl:post-parameter "target"))
                (sessionid (tbnl:cookie-in "sessionid"))
                (source-parts (extract-path-parts source))
                (target-parts (extract-path-parts target))
                ;; Outbound relationship components
                (relationship (sanitise-uid (tbnl:post-parameter "relationship")))
                (sourcepath (format nil "~{/~A~}"
                                    (append source-parts
                                            (list relationship))))
                (targetpath (format nil "~{/~A~}" target-parts))
                ;; Grab this here because it gets referred to a lot later on.
                ;; Set it to null if it doesn't have a non-empty value, because we can use that
                ;; to simplify some of the logic below.
                (return-relationship (when (and (tbnl:post-parameter "return-relationship")
                                                (not (equal "" (tbnl:post-parameter
                                                                 "return-relationship"))))
                                       (sanitise-uid (tbnl:post-parameter "return-relationship")))))
           ;; Create the relationships
           (log-message :debug (format nil "Linking ~A to ~A" sourcepath target))
           ;; Create the outbound one
           (multiple-value-bind (body status-code)
             (rg-post-json
               (rg-server tbnl:*acceptor*)
               sourcepath
               :payload `(("target" . ,targetpath))
               :sessionid sessionid)
             ;; Did that work?
             (if (< 199 status-code 300)
               ;; It worked; move on to the reverse case
               ;; First question: was a return relationship requested?
               (if return-relationship
                 ;; It was; try to create it.
                 (let ((return-sourcepath (format nil "~{/~A~}"
                                                  (append target-parts
                                                          (list return-relationship))))
                       (return-targetpath (format nil "~{/~A~}" source-parts)))
                   (log-message
                     :debug
                     (format nil "Outbound relationship successfully created. Now creating return relationship from ~A to ~A"
                             return-sourcepath return-targetpath))
                   (multiple-value-bind (return-body return-status-code)
                     (rg-post-json
                       (rg-server tbnl:*acceptor*)
                       return-sourcepath
                       :payload `(("target" . ,return-targetpath))
                       :sessionid sessionid)
                     ;; Did it work?
                     (if (< 199 return-status-code 300)
                       ;; It did; nothing more to do.
                       (tbnl:redirect (concatenate 'string "/display" source))
                       ;; It failed. Inform the user.
                       (let ((message (format
                                        nil
                                        "Failed to create return relationship ~A from ~A to '~A'. ~A: ~A"
                                        return-relationship
                                        return-sourcepath
                                        return-targetpath
                                        return-status-code
                                        return-body)))
                         (log-message :debug message)
                         (setf (tbnl:content-type*) "text/html")
                         (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                         (with-output-to-string
                           (outstr)
                           (simple-clwho-layout
                             outstr
                             :title "Failed to create return relationship"
                             :path '("Return" "Failed to create")
                             :stylesheets '("display")
                             :javascripts '("display")
                             :navbar-items (navbar-items tbnl:*acceptor*)
                             :content (with-output-to-string (content-string)
                                        (display-default
                                          content-string
                                          `(("Error:" . ,message))))))))))
                 ;; No return relationship requested; we're done here.
                 (tbnl:redirect (concatenate 'string "/display" source)))
               ;; Failed to create the outbound relationship; stopping here.
               (let ((message (format
                                nil
                                "Failed to create relationship ~A from ~A to '~A'. ~A: ~A"
                                relationship source target status-code body)))
                 (log-message :debug message)
                 (setf (tbnl:content-type*) "text/html")
                 (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                 (with-output-to-string
                   (outstr)
                   (simple-clwho-layout
                     outstr
                     :title "Failed to create outbound relationship"
                     :path '("Outbound" "Failed to create")
                     :stylesheets '("display")
                     :javascripts '("display")
                     :navbar-items (navbar-items tbnl:*acceptor*)
                     :content (with-output-to-string (content-string)
                                (display-default
                                  content-string
                                  `(("Error:" . ,message))))))))))
         ;; We didn't get those parameters
         (progn
           (setf (tbnl:content-type*) "text/html")
           (setf (tbnl:return-code*) tbnl:+http-bad-request+)
           "Need both the <code>source</code> and <code>target</code> parameters. Please try again.")))
      ;; Fallback: not implemented
      (t
        (method-not-allowed)))))
