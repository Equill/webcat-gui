;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; File upload

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun files ()
  "GUI for uploading files.
  Does not provide for fetching an existing one; for that, use Syscat's /files API."
  (cond
    ;; Either read-only mode, or session not authorised
    ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
         (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
              (not (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid")))))
     (log-message :debug "Request not permitted.")
     (tbnl:redirect "/search"))
    ;; Upload a file
    ;; On success, redirect to the item-view page.
    ((and (equal (tbnl:request-method*) :POST)
          (tbnl:post-parameter "file")
          (tbnl:post-parameter "name"))
     (log-message :debug "Received a file-upload attempt.")
     (log-message :debug (format nil "Requested filename was ~A" (tbnl:post-parameter "name")))
     (log-message :debug (format nil "Original filepath was ~A"
                                 (second (tbnl:post-parameter "file"))))
     (multiple-value-bind (response-body status-code)
       (rg-upload-file
         (rg-server tbnl:*acceptor*)
         `(("name" . ,(tbnl:post-parameter "name"))
           ;; It's already a pathname, so we just pass it on through:
           ("file" . ,(first (tbnl:post-parameter "file"))))
         :sessionid (tbnl:cookie-in "sessionid"))
       ;; Expected status code is 201; redirect accordingly.
       (if (equal 201 status-code)
         (tbnl:redirect (concatenate 'string "/display/Files/"
                                     (car (last (cl-ppcre:split "/" response-body)))))
         (tbnl:redirect (format nil "/Files?reason=~A" response-body)))))
    ;; Fail to upload a file
    ((equal (tbnl:request-method*) :POST)
     (tbnl:redirect "/files-upload/?reason=~You didn't meet a single one of the requirements."))
    ;; Display the failed-to-upload page
    ((and
       (equal (tbnl:request-method*) :GET)
       (tbnl:get-parameter "reason"))
     (progn
       (log-message :debug "Displaying the failed-to-upload page")
       (log-message :debug (format nil "Reason for failure: '~A'" (tbnl:get-parameter "reason")))
       (setf (tbnl:content-type*) "text/html")
       (setf (tbnl:return-code*) tbnl:+http-ok+)
       (with-output-to-string (outstr)
         (simple-clwho-layout
           outstr
           :title "File upload failed"
           :stylesheets '("upload")
           :path '()
           ;; Is the client authorised to make changes?
           :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                   (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                        (tbnl:cookie-in "sessionid")))
                           t)
           :navbar-items (navbar-items tbnl:*acceptor*)
           :content (cl-who:with-html-output-to-string
                      (content-string nil
                                      :prologue nil
                                      :indent t)
                      (:div :class "content"
                            (:p "Sorry, but there was a problem with uploading the file.")
                            (:p "Reason: " (princ (tbnl:get-parameter "reason") content-string))))))))
    ;; Display the file-upload form
    ((equal (tbnl:request-method*) :GET)
     (setf (tbnl:content-type*) "text/html")
     (setf (tbnl:return-code*) tbnl:+http-ok+)
     (with-output-to-string (outstr)
       (simple-clwho-layout
         outstr
         :title "File upload"
         :stylesheets '("upload" "display")
         :javascripts '("display")
         :path '("Files" "Upload")
         ;; Is the client authorised to make changes?
         :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                 (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                      (tbnl:cookie-in "sessionid")))
                         t)
         :navbar-items (navbar-items tbnl:*acceptor*)
         :content (cl-who:with-html-output-to-string
                    (content-string nil
                                    :prologue nil
                                    :indent t)
                    (:form :class "uploadform"
                           :action "/files-upload"
                           :method "post"
                           :enctype "multipart/form-data"
                           (:div :id "filename-title" "File name:")
                           (:input :type "text" :name "name" :size 128 :id "filename")
                           (:div :id "filetitle" "File:")
                           (:input :type "file" :name "file" :id "file")
                           (:div :id "submit"
                                 (:input :type "submit" :name "Upload" :value "Upload" :width "100%")))))))
    ;; Delete a file
    ((equal (tbnl:request-method*) :DELETE)
     ())
    ;; Fallback: not by this method
    (t (method-not-allowed))))
