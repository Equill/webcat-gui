;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:webcat-gui)


;;; Utility functions

(defun get-uri-parts (uri)
  "Break the URI into parts for processing by uri-node-helper.
  Expects a string; returns a list of strings."
  (declare (type string uri))
  (mapcar
    #'sanitise-uid
    ;; Remove any null elements returned by the split.
    ;; We only expect it to come from the leading / but make sure anyway.
    (remove-if #'(lambda (x)
                   (or (null x)
                       (equal x "")))
               ;; Break it up on the / delimiter
               (cl-ppcre:split "/"
                               ;; Trim off GET-style arguments
                               (first (cl-ppcre:split "\\?" uri))))))

;;; Data retrieval functions

(defun decode-json-response (json)
  "Parse the JSON returned as application/json into a CL structure"
  ;; Neo4j sends a stream of octets. Convert this into a string.
  (let ((json-string (flexi-streams:octets-to-string json :external-format :UTF-8)))
    (log-message :debug (format nil "decode-json-response received string '~A'" json-string))
    ;; If an empty string was returned, pass an empty string back.
    (if (equal json-string "")
      ""
      ;; If we received actual content, on the other hand, decode it.
      (shasht:read-json json-string))))

(defun get-cookie (name cookie-jar)
  "Return a single cookie from the jar, given its name.
  If no matching cookie is found, return nil."
  (declare (type string name)
           (type drakma:cookie-jar cookie-jar))
  (let ((cookie (car (remove-if-not (lambda (cookie)
                                      (equal name (drakma:cookie-name cookie)))
                                    (drakma:cookie-jar-cookies cookie-jar)))))
    (when cookie (drakma:cookie-value cookie))))

(defun rg-request-json (server uri
                               &key (api "raw")
                               (expected-status-code 200)
                               sessionid
                               parameters)
  "Make a GET request to a Syscat backend that should return a JSON response.
  Return the decoded JSON.
  Arguments:
  - rg-server object
  - URI
  - :api = which API to invoke: schema, files or raw (default)."
  (declare (type rg-server server)
           (type string uri api)
           (type integer expected-status-code)
           (type (or null string) sessionid)
           (type list parameters))
  (log-message :debug (format nil "Requesting URI '~A' from the ~A API" uri api))
  (let ((url (format nil "http://~A:~D~A~A"
                     (rg-server-hostname server)
                     (rg-server-port server)
                     (cond
                       ((equal "schema" api)
                        (rg-server-schema-base server))
                       ((equal "files" api)
                        (rg-server-files-base server))
                       ((equal "auth" api)
                        (rg-server-auth-base server))
                       (t   ; default is "raw"
                         (rg-server-raw-base server)))
                     ;; Auto-prepend a leading slash to the URI for a schema request
                     (if (equal "schema" api)
                       (format nil "/~A" uri)
                       uri))))
    (log-message :debug (format nil "Using URL '~A'" url))
    (log-message :debug (format nil "Using parameters '~A'" parameters))
    (let ((cookie-jar (if sessionid
                        (make-instance
                          'drakma:cookie-jar
                          :cookies (list (make-instance 'drakma:cookie
                                                        :name "sessionid"
                                                        :domain (rg-server-hostname server)
                                                        :path "/"
                                                        :value sessionid)))
                        (make-instance 'drakma:cookie-jar))))
      (multiple-value-bind (body status-code)
        (drakma:http-request
          url
          :external-format-in :UTF-8
          :cookie-jar cookie-jar
          :real-host (list (rg-server-address server)
                           (rg-server-port server))
          :parameters parameters)
        (if (equal expected-status-code status-code)
          (values (decode-json-response body) cookie-jar)
          (error 'unexpected-status-code
                 :status-code status-code
                 :text body))))))

(defun rg-delete (server uri
                         &key payload
                         schema-p
                         sessionid
                         cookie-jar)
  (declare (type rg-server server)
           (type string uri)
           (type (or null string) sessionid)
           (type (or null drakma:cookie-jar) cookie-jar))
  "Delete a resource from the Syscat backend.
  Payload format: a list of 'name=value' strings.
  Return the status code as the primary value, and the message body as an additional value.
  If both :sessionid and :cookie-jar are specified, the cookie-jar will be used and the session-id
  will be ignored."
  (let ((url (format nil "http://~A:~D~A~A?~{~A~^&~}"
                     (rg-server-hostname server)
                     (rg-server-port server)
                     (if schema-p
                       (rg-server-schema-base server)
                       (rg-server-raw-base server))
                     uri
                     payload)))
    (log-message :info (format nil "DELETEing resource with URL ~A" url))
    ;; We have three paths here:
    (let ((cookies (cond
                     ;; Client supplied a pre-made cookie-jar; use that.
                     (cookie-jar
                       cookie-jar)
                     ;; Client supplied a session ID, but no cookie-jar.
                     ;; Make one, and put the session ID in it..
                     (sessionid
                       (make-instance
                         'drakma:cookie-jar
                         :cookies (list (make-instance 'drakma:cookie
                                                       :name "sessionid"
                                                       :domain (rg-server-hostname server)
                                                       :path "/"
                                                       :value sessionid))))
                     ;; Fine, we'll do it ourselves. No cookie, though.
                     (t
                       (make-instance 'drakma:cookie-jar)))))
      (multiple-value-bind (body status-code)
        (drakma:http-request url
                             :method :DELETE
                             :external-format-in :UTF-8
                             :cookie-jar cookies
                             :real-host (list (rg-server-address server)
                                              (rg-server-port server)))
        (values status-code body)))))

(defun post-encode-payload (payload)
  "Transform a list of dotted pairs of strings into a POST-encoded string.
   The intent is to provide Drakma with pre-encoded content,
   instead of having it URL-encode the content of a PUT request.
   This also bypasses the 1024-character limit that comes with URL-encoding."
  (format nil "~{~A~^&~}"
          (mapcar #'(lambda (pair)
                      (format nil "~A=~A"
                              (drakma:url-encode (car pair) :UTF-8)
                              (drakma:url-encode (cdr pair) :UTF-8)))
                  payload)))

(defun rg-upload-file (server payload &key sessionid)
  "Upload a file to a Syscat backend."
  (declare (type rg-server server)
           (type (or null string) sessionid))
  (log-message :debug (format nil "Uploading a file"))
  (let ((cookie-jar (if sessionid
                      (make-instance
                        'drakma:cookie-jar
                        :cookies (list (make-instance 'drakma:cookie
                                                      :name "sessionid"
                                                      :domain (rg-server-hostname server)
                                                      :path "/"
                                                      :value sessionid)))
                      (make-instance 'drakma:cookie-jar))))
    (multiple-value-bind (body status-code headers)
      (drakma:http-request
        (format nil "http://~A:~D~A~A"
                (rg-server-hostname server)
                (rg-server-port server)
                (rg-server-files-base server)
                "/files/v1")
        :parameters payload
        :form-data t
        :method :POST
        :external-format-in :UTF-8
        :external-format-out :UTF-8
        :cookie-jar cookie-jar
        :real-host (list (rg-server-address server)
                         (rg-server-port server)))
      ;; Now decide what to do with it.
      ;; If it was successful, return it
      (log-message :debug (format nil "Response status-code: ~A" status-code))
      (log-message :debug (format nil "Response headers: ~A" headers))
      (if (and (> status-code 199)
               (< status-code 300))
        ;; Success!
        (progn
          (log-message :debug "Request succeeded.")
          (values
            (if (equal (cdr (assoc :content-type headers))
                       "application/json")
              ;; If it's JSON, decode it
              (decode-json-response body)
              ;; If not JSON, just return the body
              body)
            status-code))
        ;; Failure!
        (progn
          (log-message :debug (format nil "Failure: ~A ~A" status-code body))
          (values body status-code))))))

(defun rg-post-json (server uri &key payload
                            put-p
                            (api "raw")
                            sessionid)
  "Make a POST request to a Syscat backend, and decode the JSON response if there was one.
  Arguments:
  - rg-server object
  - URI
  - :payload = Drakma-ready alist of values to POST
  - :put-p = whether we're invoking the PUT method
  - :api = whether to invoke the schema, files or raw (default) API
  - :cookie-jar = If you already have a cookie-jar to use across requests, supply it here.
  Return the body as the primary value, and the status-code and cookie-jar as secondary values."
  (declare (type rg-server server)
           (type string uri)
           (type boolean put-p)
           (type (or null string) api sessionid))
  (log-message :debug (format nil "rg-post-json received payload: ~A" payload))
  (let ((encoded-payload (post-encode-payload payload))
        (method (if put-p :PUT :POST))
        (url (format nil "http://~A:~D~A~A"
                     (rg-server-hostname server)
                     (rg-server-port server)
                     (cond
                       ((equal "schema" api)
                        (rg-server-schema-base server))
                       ((equal "files" api)
                        (rg-server-files-base server))
                       ((equal "auth" api)
                        (rg-server-auth-base server))
                       (t   ; default is "raw"
                         (rg-server-raw-base server)))
                     uri))
        (cookie-jar (if sessionid
                      (make-instance
                        'drakma:cookie-jar
                        :cookies (list (make-instance 'drakma:cookie
                                                      :name "sessionid"
                                                      :domain (rg-server-hostname server)
                                                      :path "/"
                                                      :value sessionid)))
                      (make-instance 'drakma:cookie-jar))))
    (log-message :debug (format nil "~Aing a request to URI ~A" method url))
    (log-message :debug (format nil "Encoded payload: ~A" encoded-payload))
    (multiple-value-bind (body status-code headers)
      (drakma:http-request
        url
        :real-host (list (rg-server-address server)
                         (rg-server-port server))
        :content encoded-payload
        :form-data (member api '("files" "schema") :test #'equal)
        :method method
        :external-format-in :UTF-8
        :external-format-out :UTF-8
        :cookie-jar cookie-jar)
      ;; Now decide what to do with it.
      ;; If it was successful, return it
      (log-message :debug (format nil "Response status-code: ~A" status-code))
      (log-message :debug (format nil "Response headers: ~A" headers))
      (if (and (> status-code 199)
               (< status-code 300))
        ;; Success!
        (progn
          (log-message :debug "Request succeeded.")
          (values
            (if (equal (cdr (assoc :content-type headers))
                       "application/json")
              ;; If it's JSON, decode it
              (decode-json-response body)
              ;; If not JSON, just return the body
              body)
            status-code
            cookie-jar))
        ;; Failure!
        (progn
          (log-message :debug (format nil "Failure: ~A ~A" status-code body))
          (values body status-code cookie-jar))))))

(defun search-for-resources (server rtype &key params sessionid)
  "Search in the backend for a thing.
  Expected to be called from the search page.
  params = optional list of strings."
  (declare (type rg-server server)
           (type string rtype)
           (type list params)
           (type (or null string) sessionid))
  (log-message :debug (format nil "Searching for ~A with parameters ~A" rtype params))
  (let ((query-string (format nil "/~A" rtype)))
    (log-message :debug (format nil "search-for-resources using query string '~A'" query-string))
    (let ((result (rg-request-json server
                                   query-string
                                   :sessionid sessionid
                                   :parameters params)))
      (if (equal result "")
        nil
        result))))

(defun search-results-to-template (res)
  "Transform the output of search-for-resources into something digestible by html-template.
  Accepts a list of alists.
  Returns a list of plists, with keys:
  - :uid
  - :title = UID after escaping via uid-to-title to be more human-friendly.
  Only pulls out the UID because that's the only attribute we can expect to get.
  We're currently searching specifically by resourcetype, so we already know that and
  it's not in the returned results anyway."
  (log-message :debug (format nil "search-results-to-template formatting results ~A" res))
  (mapcar #'(lambda (result)
              `(:uid ,(gethash "ScUID" result)
                     :title  ,(uid-to-title (gethash "ScUID" result))))
          (sort res #'(lambda (row1 row2)
                        (string< (gethash "ScUID" row1)
                                 (gethash "ScUID" row2))))))

#|
This seems really stupid, but it should work.
From inside to outside:
- replace __ with / to put it safely out of the way.
- replace _ with a space.
- replace / with _.
Now double-underscores have turned into single underscores,
single underscores have turned into spaces,
and any forward-slashes that sneaked through are also now underscores.
|#
(defun uid-to-title (uid)
  "Render the UID as a human-friendly title string"
  (cl-ppcre:regex-replace-all
    "/"
    (cl-ppcre:regex-replace-all
      "_"
      (cl-ppcre:regex-replace-all "__" uid "/")
      " ")
    "_"))

(defun format-timestamp (stamp)
  "Take an integer timestamp, and return an ISO-8601-style string."
  (declare (type integer stamp))
  (multiple-value-bind (sec minute hour date month year)
    (decode-universal-time stamp)
    (format nil "~D:~2,'0D:~2,'0D ~2,'0D:~2,'0D:~2,'0D"
            year month date hour minute sec)))

(defun get-comments (server resource-path &key sessionid)
  "Fetch all comments attached to the resource at this path."
  (declare (type rg-server server)
           (type string resource-path)
           (type (or null string) sessionid))
  (mapcar #'(lambda (comment)
              (make-instance 'comment
                             :uid (gethash "ScUID" comment)
                             :createddate (gethash "ScCreateddate" comment)
                             :text (gethash "text" comment)))
          (rg-request-json server
                           (format nil "~A/HAS_COMMENT/Comments" resource-path)
                           :sessionid sessionid
                           :parameters '(("SCattributes" . "text")))))

(defun make-simple-alist (lst tag)
  "Turn a list of atoms into a list of '(,tag <atom>) lists"
  (mapcar #'(lambda (atm)
              (list tag atm))
          lst))

(defun search-for-tasks (server &key
                                statuses
                                scale
                                urgency
                                importance
                                uid-regex
                                sessionid)
  "Search for tasks using the requested parameters.
  Return a plist:
  :uid
  :scale
  :importance
  :urgency
  :status"
  (declare (type rg-server server)
           (type list statuses scale urgency importance)
           (type (or null string) uid-regex sessionid))
  (log-message :debug (format nil "Searching for tasks with importances ~{~A~^, ~}, urgencies ~{~A~^ ~} and statuses ~{~A~^, ~}"
                              (or importance '("<any>"))
                              (or urgency '("<any>"))
                              (or statuses '("<any>"))))
  (let ((query-string "/Tasks")
        (params '(("SCattributes" . "scale,importance,urgency,status,deadline"))))
    (when statuses (push `("status" . ,(format nil "~{~A~^,~}" statuses)) params))
    (when scale (push `("scale" . ,(format nil "~{~A~^,~}" scale)) params))
    (when urgency (push `("urgency" . ,(format nil "~{~A~^,~}" urgency)) params))
    (when importance (push `("importance" . ,(format nil "~{~A~^,~}" importance)) params))
    (when uid-regex (push `("uid-regex" . ,uid-regex) params))
    (log-message :debug (format nil "Using query URL '~A'" query-string))
    (log-message :debug (format nil "Using parameters ~A" params))
    ;; Transform the results into an alist
    (let ((raw-results (mapcar
                         #'(lambda (row)
                             ;; Eliminate WTF sort-order by removing spurious characters
                             ;; that are prone to creeping in, like leading/trailing spaces
                             ;; and quote-marks.
                             (let* ((uid (gethash "ScUID" row))
                                    (title (uid-to-title uid))
                                    (scale (gethash "scale" row))
                                    (scale-numeric (cond ((equal "high" scale) 1)
                                                         ((equal "medium" scale) 2)
                                                         (t 3)))
                                    (importance (gethash "importance" row))
                                    (importance-numeric (cond ((equal "high" importance) 1)
                                                              ((equal "medium" importance) 2)
                                                              (t 3)))
                                    (urgency (gethash "urgency" row))
                                    (urgency-numeric (cond ((equal "high" urgency) 1)
                                                           ((equal "medium" urgency) 2)
                                                           (t 3)))
                                    (status (gethash "status" row))
                                    ;; Filter out deadlines that were recorded as an empty string
                                    (deadcandidate (gethash "deadline" row))
                                    (deadline (when (and deadcandidate
                                                         (stringp deadcandidate)
                                                         (not (equal "" deadcandidate)))
                                                deadcandidate)))
                               (list :uid uid
                                     :title title
                                     :scale scale
                                     :scale-numeric scale-numeric 
                                     :importance importance
                                     :importance-numeric importance-numeric
                                     :urgency urgency
                                     :urgency-numeric urgency-numeric
                                     :status status
                                     :deadline deadline)))
                         (rg-request-json server
                                          query-string
                                          :sessionid sessionid
                                          :parameters params))))
      (log-message :debug (format nil "Raw results of task-search: ~A" raw-results))
      ;; Sort the alist and return it.
      ;; Note that `stable-sort` is required, to prevent each iteration from scrambling its predecessor's results
      (stable-sort
        (stable-sort
          (stable-sort
            (sort raw-results #'> :key #'(lambda (item) (getf item :scale-numeric)))
            #'< :key #'(lambda (item) (getf item :importance-numeric)))
          #'< :key #'(lambda (item) (getf item :urgency-numeric)))
        #'string< :key (lambda (item) (getf item :deadline))))))


(defun get-enum-vals (attr attrlist)
  "Extract the enum values for an attribute identified,
  from the alist of schema-rtype-attr structs returned by get-attrs-with-keywords.
  It's possible it returns a list of strings."
  (declare (type string attr)
           (type list attrlist))
  (log-message :debug (format nil "Extracting values for '~A' from attribute-list ~A" attr attrlist))
  (attrvalues
    (car (remove-if-not #'(lambda (attrdef)
                            (equal attr (name attrdef)))
                        attrlist))))

(defun sanitise-uid (uid)
  "Replace UID-unfriendly characters in UIDs with something safe.
   Expects a string and returns another string."
  (cl-ppcre:regex-replace-all "[/ ]" uid "_"))

(defun get-sub-uri (uri base-uri)
  "Extract the URI from the full request string,
   excluding the base URL and any GET parameters.
   Expects two strings; returns one string."
  (first (cl-ppcre:split "\\?" (cl-ppcre:regex-replace base-uri uri ""))))

(defun filter-params (name params)
  "Filter Hunchentoot parameters for those matching a specific name."
  (remove-if #'(lambda (param)
                 (or (null param)
                     (equal param "")))
             (mapcar #'(lambda (par)
                         (when (equal (car par) name) (cdr par)))
                     params)))

(defun confirm-rg-is-running (server &key (counter 1) (max-count 5) (sleep-time 5))
  "Wait until we're sure the Syscat server is accepting requests.
   Or time out. Whichever."
  (declare (type rg-server server))
  (log-message :debug
               (format nil "Confirming whether the Syscat server is listening on ~A:~A"
                       (rg-server-address server) (rg-server-port server)))
  (handler-case
    (multiple-value-bind (body status-code)
      (drakma:http-request (format nil "http://~A:~A/healthcheck" (rg-server-hostname server)
                                   (rg-server-port server))
                           :real-host (list (rg-server-address server)
                                            (rg-server-port server)))
      ;; If we got here, we got _a_ response from the server
      (if (equal status-code 200)
          t
          (error (format nil "Non-OK server response: ~D - ~A" status-code body))))
    ;; Not listening. Wait however long, then try again
    (usocket:connection-refused-error
      (e)
      (declare (ignore e))
      (if (>= counter max-count)
          ;; Timed out.
          ;; Leave a message, then exit this whole thing.
          (progn
            (log-message :crit "Timed out trying to connect to the server. Exiting.")
            (sb-ext:exit))
          ;; Still isn't responding, but we haven't yet hit timeout.
          ;; Leave a message, pause, then try again.
          (progn
            (log-message :warn (format nil "Connection refused. Pausing for ~A seconds before retrying"
                                       sleep-time))
            (sleep sleep-time)
            (confirm-rg-is-running server
                                   :counter (+ counter 1)
                                   :max-count max-count
                                   :sleep-time sleep-time))))))

(defun canonical-path-p (server path)
  "Verify whether the supplied URI is canonical, i.e. uniquely identifies a single resource.
  The path is expected to be a list of strings, already processed via `get-uri-parts`."
  (declare (type rg-server server)
           (type list path))
  (let ((uri (format nil "~{/~A~}" path)))
    (log-message :debug (format nil "Checking whether path is canonical: /~A" uri))
    ;; Apply sanity checks before proceeding
    (when
      ;; Is it the right length?
      (= (mod (length path) 3) 2)
      ;; Default case: it looks plausible, so we're trying it out.
      (equal uri
             (gethash "ScCanonicalpath" (first (rg-request-json server uri)))))))
