;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Configs for the server to use

(in-package #:webcat-gui)

;; Enable extra markdown options
(setf 3bmd-code-blocks:*code-blocks* t)
(setf 3bmd-definition-lists:*definition-lists* t)
(setf 3bmd-tables:*tables* t)
;(setf 3bmd-ext-math:*math* t)

;; Stop the compiler complaining about an undefined variable
(defvar *webcat-gui-acceptor* nil)

;; Shasht configuration
(setf shasht:*read-default-null-value* nil)
(setf shasht:*read-default-array-format* :list)
