;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Create a new resource

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun create-item ()
  "Display the create-item page"
  (log-message :debug "Dispatching the create-item page.")
  (let ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid"))))
    (cond
      ;; Display the create-item page
      ((equal (tbnl:request-method*) :GET)
       (log-message :debug (format nil "Handling create GET request ~A" (tbnl:request-uri*)))
       (let ((resourcetype-names
               (remove-if
                 #'(lambda (rtype)
                     (or (equal "any" rtype)
                         (dependent (gethash rtype (schema tbnl:*acceptor*)))))
                 (get-resourcetypes (schema tbnl:*acceptor*)))))
         (setf (tbnl:content-type*) "text/html")
         (setf (tbnl:return-code*) tbnl:+http-ok+)
         (with-output-to-string (outstr)
           (simple-clwho-layout
             outstr
             :title "Create item"
             :stylesheets '("create" "display")
             :javascripts '("search")
             :path (list (or (tbnl:get-parameter "resourcetype") "Create")
                         (or (tbnl:get-parameter "ScUID") ""))
             ;; Is the client authorised to make changes?
             :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                     (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                          sessionid))
                             t)
             :navbar-items (navbar-items tbnl:*acceptor*)
             :content
             (cond
               ;; If the client won't be able to create the item, tell them that now.
               ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
                    (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                         (not sessionid)))
                ;; Can't create; tell them that.
                (cl-who:with-html-output-to-string
                  (content-string nil
                                  :prologue nil
                                  :indent t)
                  (:p "You are not authorised to create an item. Either the site is in read-only mode, or you need to log in first.")))
               ;; Resourcetype isn't specified; present that choice before continuing.
               ((not (gethash (tbnl:get-parameter "resourcetype") (schema tbnl:*acceptor*)))
                (cl-who:with-html-output-to-string
                   (content-string nil
                                   :prologue nil
                                   :indent t)
                   (:form :action "/create"
                          :method :GET
                          :class "formgrid-create"
                          :id "createform"
                          ;; Resource-types
                          (:div :class "header" :id "header-resourcetypes" "Resource type")
                          (:select :name "resourcetype"
                                   :id "resourcetype"
                                   :size 15
                                   (loop for rtype in resourcetype-names
                                         do (if (equal rtype (tbnl:get-parameter "resourcetype"))
                                              (cl-who:htm (:option :value rtype
                                                                   :selected "selected"
                                                                   (princ rtype content-string)))
                                              (cl-who:htm (:option :value rtype
                                                                   (princ rtype content-string))))))
                          ;; Submit button
                          (:input :type "submit" :value "Create" :id "createbutton"))))
               ;; Resourcetype is specified. Get the rest of the details
               (t
                 (cl-who:with-html-output-to-string
                   (content-string nil
                                   :prologue nil
                                   :indent t)
                   (:form :action "/create"
                          :method "post"
                          :class "formgrid-create"
                          :id "createform"
                          ;; Attributes
                          (:table :id "table-attributes"
                                  (:tr (:td :class "attrnames" "UID")
                                       (:td :class "attrvals"
                                            (:input :type "text"
                                                    :size 80
                                                    :name "ScUID"
                                                    :id "input-uid"
                                                    :autofocus "true"
                                                    :value (or (tbnl:get-parameter "ScUID") ""))))
                                  (loop for attr in (attributes
                                                      (gethash
                                                        (tbnl:get-parameter "resourcetype")
                                                        (schema tbnl:*acceptor*)))
                                        do (cl-who:htm
                                             (:tr (:td :class "attrnames"
                                                       :title (description attr)
                                                       (princ (name attr) content-string))
                                                  (:td :class "attrvals"
                                                       ;; Present input fields that match the type
                                                       (cond
                                                         ;; Read-only attrs
                                                         ((readonly attr)
                                                          "(Read-only attribute)")
                                                         ;; Text attrs get a textarea
                                                         ((typep attr 'schema-rtype-attr-text)
                                                          (cl-who:htm (:textarea :name (name attr))))
                                                         ;; Boolean
                                                         ((typep attr 'schema-rtype-attr-boolean)
                                                          (cl-who:htm (:input
                                                                        :name (name attr)
                                                                        :type "checkbox")))
                                                         ;; Integer
                                                         ((typep attr 'schema-rtype-attr-integer)
                                                          (cl-who:htm (:input
                                                                        :name (name attr)
                                                                        :type "number")))
                                                         ;; Varchar with a set of values
                                                         ((and (typep attr 'schema-rtype-attr-varchar)
                                                               (attrvalues attr))
                                                          (cl-who:htm (:select
                                                                        :name (name attr)
                                                                        (mapcar (lambda (val)
                                                                                  (cl-who:htm
                                                                                    (:option
                                                                                      :value val
                                                                                      (princ val content-string))))
                                                                                (attrvalues attr)))))
                                                         ;; Varchar with a set length
                                                         ((and (typep attr 'schema-rtype-attr-varchar)
                                                               (integerp (maxlength attr))
                                                               (> (maxlength attr) 0))
                                                          (cl-who:htm (:input
                                                                        :name (name attr)
                                                                        :type "text"
                                                                        :length (maxlength attr)
                                                                        :maxlength (maxlength attr)
                                                                        :size (min (maxlength attr) 80))))
                                                         ;; Default assumption is that it's a text field
                                                         (t
                                                           (cl-who:htm (:input
                                                                         :name (name attr)
                                                                         :type "text"
                                                                         :size 80)))))))))
                          ;; Remember to mention the resourcetype
                          (:input :name "resourcetype"
                                  :type "hidden"
                                  :value (princ (tbnl:get-parameter "resourcetype") content-string))
                          ;; Submit button
                          (:input :type "submit" :value "Create" :id "createbutton")))))))))
      ;; POST request but either read-only mode, or session not authorised
      ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
           (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                (not sessionid)))
       (return-client-error "Request not permitted."))
      ;; POST request: create a resource
      ((equal (tbnl:request-method*) :POST)
       (let ((uid (tbnl:post-parameter "ScUID"))
             (resourcetype (tbnl:post-parameter "resourcetype")))
         (log-message :debug (format nil "Handling create POST request ~A" (tbnl:request-uri*)))
         ;; Check for the UID and resourcetype; if we don't have those, give up now
         ;; Missing both of them
         (cond ((and (or (not uid)
                         (equal uid ""))
                     (or (not uid)
                         (equal uid "")))
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                "Both the UID and the resourcetype must be specified")
               ;; Missing only the UID
               ((or (not uid)
                    (equal uid ""))
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                "The ScUID parameter must be supplied")
               ;; Missing only the resourcetype
               ((or (not resourcetype)
                    (equal resourcetype ""))
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                "The resourcetype must be specified in the URL")
               ;; We have both; carry on
               (t
                 ;; Extract non-empty attributes relevant to this resourcetype
                 (let* ((valid-attrnames
                          (mapcar #'name
                                  (get-attrs (schema tbnl:*acceptor*) resourcetype)))
                        (validated-attrs
                          (remove-if #'null
                                     (mapcar #'(lambda (param)
                                                 (log-message
                                                   :debug
                                                   (format nil "Validating parameter ~A" (car param)))
                                                 (when (and (not (or (null (cdr param))
                                                                     (equal "" (cdr param))))
                                                            (member (car param)
                                                                    valid-attrnames
                                                                    :test #'equal))
                                                   param))
                                             (tbnl:post-parameters*)))))
                   (log-message :debug (format nil "Validated attributes: ~A" validated-attrs))
                   ;; Send the update
                   (multiple-value-bind (body status-code)
                     (rg-post-json (rg-server tbnl:*acceptor*)
                                   (concatenate 'string "/" resourcetype)
                                   :payload (append `(("ScUID" . ,(sanitise-uid uid))) validated-attrs)
                                   :sessionid sessionid)
                     ;; Did it work?
                     (if (and (> status-code 199)
                              (< status-code 300))
                       ;; Happy path
                       (tbnl:redirect (concatenate 'string "/display/" resourcetype "/" (sanitise-uid uid)))
                       ;; Less-happy path
                       (with-output-to-string (outstr)
                         (setf (tbnl:content-type*) "text/html")
                         (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                         (simple-clwho-layout
                           outstr
                           :title (format nil "Failed to create ~A '~A'" resourcetype uid)
                           ;; Is the client authorised to make changes?
                           :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                                   (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                                        sessionid))
                                           t)
                           :path (list resourcetype uid)
                           :stylesheets '("display")
                           :javascripts '("display")
                           :content (with-output-to-string (content-string)
                                      (display-default
                                        content-string
                                        `(("Server message" . ,body)))))))))))))
      ;; Fallback: not by this method
      (t (method-not-allowed)))))
