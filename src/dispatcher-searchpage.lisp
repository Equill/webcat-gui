;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Display an individual resource

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun searchpage ()
  "Display the search-page"
  (cond
    ((equal (tbnl:request-method*) :GET)
     (log-message :debug "Displaying the search page.")
     (let* ((sessionid (tbnl:cookie-in "sessionid"))
            ;; Here we define "schema" as a plist with attributes :name and :selected
            (schema (mapcar #'(lambda (rtype)
                                (log-message :debug (format nil "Examining resourcetype ~A" rtype))
                                (list :name rtype
                                      :selected (when (equal rtype (tbnl:get-parameter "resourcetype"))
                                                  "selected")))
                            (remove-if
                              #'(lambda (rtype)
                                  (or (equal "any" rtype)
                                      (dependent (gethash rtype (schema tbnl:*acceptor*)))))
                              (get-resourcetypes (schema tbnl:*acceptor*)))))
            (tbnl-formatted-results
              (if (tbnl:get-parameter "resourcetype")
                (search-results-to-template
                  (let* ((requested-attributes
                           (remove-if #'null
                                      (mapcar #'(lambda (attr)
                                                  (let ((val (tbnl:get-parameter attr)))
                                                    (when val (cons attr val))))
                                              (mapcar #'name
                                                      (get-attrs (schema tbnl:*acceptor*)
                                                                 (tbnl:get-parameter "resourcetype"))))))
                         (search-criteria (append ()
                                                  (when (tbnl:get-parameter "uid_regex")
                                                    `(("ScUID" . ,(tbnl:get-parameter "uid_regex")))))))
                    (progn
                      (log-message :debug (format nil "Searching with criteria '~A'" search-criteria))
                      (search-for-resources (rg-server tbnl:*acceptor*)
                                            (tbnl:get-parameter "resourcetype")
                                            :params (append search-criteria
                                                            requested-attributes)
                                            :sessionid sessionid))))
                ;; If no resourcetype was specified, tbnl-formatted-results is NIL:
                ())))
       ;; Debug logging for what we've obtained so far
       (log-message :debug (format nil "Resourcetype list: ~A" schema))
       (log-message :debug (format nil "Resourcetype supplied: ~A"
                                   (if (tbnl:get-parameter "resourcetype")
                                     (tbnl:get-parameter "resourcetype")
                                     "none")))
       (log-message :debug (format nil "tbnl-formatted search results: ~A" tbnl-formatted-results))
       (setf (tbnl:content-type*) "text/html")
       (setf (tbnl:return-code*) tbnl:+http-ok+)
       (with-output-to-string (outstr)
         (simple-clwho-layout
           outstr
           :title "Webcat search page"
           :path '("Search")
           :stylesheets '("search" "display")
           :javascripts '("search")
           ;; Is the client authorised to make changes?
           :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                   (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                        sessionid))
                           t)
           :navbar-items (navbar-items tbnl:*acceptor*)
           :content
           (cl-who:with-html-output-to-string
             (content-string nil
                             :prologue nil
                             :indent t)
             (:div :id "searchbox"
                   (:form ;:id "searchform"
                     :class "formgrid-search"
                     :method "get"
                     :action "/search"
                     ;; Resource types
                     (:div :id "resourcetypes"
                           (:h3 "Resource types")
                           (:select :name "resourcetype" :id "resourcetype" :size 15
                                    (loop for rtype in schema
                                          do (if (getf rtype :selected)
                                               (cl-who:htm (:option :value (getf rtype :name)
                                                                    :selected "selected"
                                                                    (princ (getf rtype :name) content-string)))
                                               (cl-who:htm (:option :value (getf rtype :name)
                                                                    (princ (getf rtype :name) content-string)))))))
                     ;; Javascript-updated search refinement
                     (:div :id "refine-title"
                           (:h3 "Refine your search"))
                     (:div :id "attrnames" :class "attrnames"
                           (:div :id "refinesearch" "UID regex"))
                     (:div :id "attrvals" :class "attrvals"
                           (:input :type "text"
                                   :name "uid_regex"
                                   :id "refinesearch-input"
                                   :value (or (tbnl:get-parameter "uid_regex") "")))
                     ;; Submit button
                     (:div :id "submit"
                           (:input :type "submit" :id "searchbutton" :value "Update search")))
                   ;; Search results
                   (:div :id "searchresults"
                         (:h2 "Search results")
                         (:ul :class "results-list"
                              (loop for result in tbnl-formatted-results
                                    do (cl-who:htm
                                         (:li (:a :href (format nil "display/~A/~A"
                                                                (tbnl:get-parameter "resourcetype")
                                                                (getf result :uid))
                                                  (format content-string "/~A/~A"
                                                          (tbnl:get-parameter "resourcetype")
                                                          (getf result :uid))))))))))))))
    ;; Fallback: not by this method
    (t (method-not-allowed))))
