;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Login, logout and any other auth-related manoeuvres

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defun display-mgmt-panel (&key acceptor message sessionid username)
  "Render the management panel"
  (setf (tbnl:content-type*) "text/html")
  (setf (tbnl:return-code*) tbnl:+http-ok+)
  (with-output-to-string (outstr)
    (simple-clwho-layout
      outstr
      :title "Management"
      :path (list "Manage")
      :stylesheets '("management")
      ;; Is the client authorised to make changes?
      :authorised-p (when (or (equal (auth-policy acceptor) "open")
                              (and (equal (auth-policy acceptor) "authenticated")
                                   sessionid))
                      t)
      :navbar-items (navbar-items acceptor)
      :content (cl-who:with-html-output-to-string
                 (content-string nil
                                 :prologue nil
                                 :indent t)
                 (:div :class "content"
                  (:div :class "account-mgmt"
                   (:div :class "message" (cond
                                            (message
                                              (princ message content-string))
                                            (username
                                              (format content-string
                                                      "Account management for ~A"
                                                      username))
                                            (t
                                             (princ "Management page" content-string))))
                   ;; Add new account
                   (when (equal username "ScAdmin")
                     (cl-who:htm
                       (:form :method "POST"
                        :class "add-user"
                        (:div :class "useradd-header"
                         (:hr)
                         "Add new account")
                        (:div :class "useradd-uid-title" "New user's UID:")
                        (:input :name "new-user-uid"
                         :type "text"
                         :class "useradd-uid-field")
                        (:div :class "useradd-passwd1-title"
                         "New user's password:")
                        (:input :name "new-user-passwd1"
                         :type "password"
                         :class "useradd-passwd1-field")
                        (:div :class "useradd-passwd2-title"
                         "Confirm password:")
                        (:input :name "new-user-passwd2"
                         :type "password"
                         :class "useradd-passwd2-field")
                        (:input :name "submit-new-user"
                         :type "submit"
                         :class "useradd-submit"
                         :value "Create new user"))))
                   ;; Manage existing accounts
                   (when (equal username "ScAdmin")
                     (let ((users (sort
                                    (remove-if
                                              (lambda (account) (equal "ScAdmin" (gethash "name" account)))
                                              (rg-request-json (rg-server acceptor)
                                                               "/accounts"
                                                               :api "auth"
                                                               :sessionid sessionid))
                                    #'string<
                                    :key (lambda (element) (gethash "name" element)))))
                       (cl-who:htm
                         (:form :method "POST"
                          :class "manage-users"
                          (:div :class "usermgr-title"
                           (:hr)
                           "Manage existing accounts:")
                          (:div :class "deactivate-title" "Active users to deactivate:")
                          (:select :name "deactivate-account"
                           :class "deactivate"
                           :multiple "multiple"
                           :size 10
                           (loop for account
                                 in (remove-if-not
                                      ;; This works because the value is interpreted
                                      ;; as a CL boolean.
                                      (lambda (user) (gethash "active" user))
                                      users)
                                 do (let ((name (gethash "name" account)))
                                      (cl-who:htm (:option :value name
                                                   (princ name content-string))))))
                          (:div :class "activate-title" "Deactivated users to reactivate:")
                          (:select :name "activate-account"
                           :class "activate"
                           :multiple "multiple"
                           :size 10
                           (loop for account
                                 in (remove-if
                                              (lambda (user)
                                                ;; This works because the value is interpreted
                                                ;; as a CL boolean.
                                                (gethash "active" user))
                                              users)
                                 do (let ((name (gethash "name" account)))
                                      (cl-who:htm (:option :value name
                                                   (princ name content-string))))))
                          (:input :type "submit"
                           :name "manage-users"
                           :class "usermgr-submit"
                           :value "Manage user status")))))
                   ;; Password-change form
                   (when (equal "authenticated" (auth-policy acceptor))
                     (cl-who:htm
                       (:form :method "POST"
                        :class "password-change"
                        (:div :class "pwc-header"
                         (:hr)
                         "Change my password")
                        (:div :class "pwc-pwd1-title" "New password:")
                        (:input :name "password1"
                         :type "password"
                         :class "pwc-pwd1-field")
                        (:div :class "pwc-pwd2-title" "Confirm new password:")
                        (:input :name "password2"
                         :type "password"
                         :class "pwc-pwd2-field")
                        (:input :type "submit"
                         :class "pwc-pwd-submit"
                         :value "Change password"))))
                   ;; Schema file-upload
                   (when (or (equal username "ScAdmin")
                             (equal (auth-policy acceptor) "open"))
                     (cl-who:htm
                       (:form :method "POST"
                        :enctype "multipart/form-data"
                        :class "schema-upload"
                        (:div :class "schema-upload-header"
                         "Upload new subschema")
                        (:input :name "schemafile"
                         :type "file"
                         :class "file-selector")
                        (:div :class "create-new-schema"
                         (:p "Create new schema-version?")
                         (:input :name "create-new-schema"
                          :type "checkbox"))
                        (:input :type "submit"
                         :class "schema-upload-submit"
                         :value "Upload subschema"))))
                   ;; Logout form
                   (when (equal "authenticated" (auth-policy acceptor))
                     (cl-who:htm
                       (:form :class "logout-form"
                        :method "POST"
                        (:hr)
                        (:input :type "hidden"
                         :name "logout"
                         :value "true")
                        (:input :class "submit"
                         :type "submit"
                         :value "Log out"))))))))))

(defun management ()
  "Site- and account-management page"
  (log-message :debug "Dispatching the auth page.")
  (let* ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                      (tbnl:cookie-in "sessionid")))
         (username (when sessionid
                     (handler-case
                       (let ((result (rg-request-json (rg-server tbnl:*acceptor*)
                                                      "/sessions"
                                                      :api "auth"
                                                      :sessionid sessionid)))
                         (when result (gethash "username" result)))
                       (unexpected-status-code
                         (e)
                         (declare (ignore e))
                         (log-message :debug "Unexpected status code from Syscat server. Most likely means no session."))
                       (error (e) (log-message :error (format nil "~A" e)))))))
    (cond
      ;; POST request with credentials
      ((and
         (equal "authenticated" (auth-policy tbnl:*acceptor*))
         (equal (tbnl:request-method*) :POST)
         (tbnl:post-parameter "username")
         (tbnl:post-parameter "password"))
       (let ((username (sanitise-uid (tbnl:post-parameter "username"))))
         (log-message :debug (format nil "Handling login attempt for username '~A'" username))
         ;; Authenticate the user
         (multiple-value-bind (body status-code cookie-jar)
           (rg-post-json (rg-server tbnl:*acceptor*)
                         "/sessions"
                         :api "auth"
                         :payload `(("username" . ,username)
                                    ("password" . ,(tbnl:post-parameter "password"))))
           (declare (ignore body))
           (if (and (> status-code 199)
                    (< status-code 300))
               ;; Authentication succeeded
               (progn
                 (log-message :debug "Login successful")
                 (log-message :debug (format nil "Received sessionid cookie with value '~A'"
                                             (get-cookie "sessionid" cookie-jar)))
                 (log-message :debug (format nil "Contents of entire cookie-jar: ~A" cookie-jar))
                 (setf (tbnl:content-type*) "text/html")
                 (setf (tbnl:return-code*) tbnl:+http-ok+)
                 (tbnl:set-cookie "sessionid"
                                  :domain (cookie-domain tbnl:*acceptor*)
                                  :path "/"
                                  :value (get-cookie "sessionid" cookie-jar))
                 (tbnl:redirect "/management"))
               ;; Authentication failed
               (tbnl:redirect "/management?message=Those credentials weren't valid. Please try again.")))))
      ;; POST request for password-change
      ((and
         (equal "authenticated" (auth-policy tbnl:*acceptor*))
         (equal (tbnl:request-method*) :POST)
         (tbnl:post-parameter "password1")
         (tbnl:post-parameter "password2"))
       (log-message :debug "Handling password-change request")
       ;; Sanity-check: did they type the same password twice?
       (if (equal (tbnl:post-parameter "password1")
                  (tbnl:post-parameter "password2"))
           ;; Sanity-check passed. Try to change the password.
           (multiple-value-bind (body status-code)
             (rg-post-json (rg-server tbnl:*acceptor*)
                           "/accounts"
                           :api "auth"
                           :payload `(("password" . ,(tbnl:post-parameter "password1")))
                           :put-p t
                           :sessionid sessionid)
             (if (equal status-code tbnl:+http-created+)
                 (tbnl:redirect "/management?message=Password changed successfully")
                 (tbnl:redirect (format nil "/management?message=~A" body))))
           ;; Sanity-check failed. Tell them to try again, but better.
           (tbnl:redirect "/management?message=Passwords don't match. Please try again.")))
      ;; POST request to create new account
      ((and
         (equal "authenticated" (auth-policy tbnl:*acceptor*))
         (equal (tbnl:request-method*) :POST)
         (tbnl:post-parameter "new-user-uid")
         (tbnl:post-parameter "new-user-passwd1")
         (tbnl:post-parameter "new-user-passwd2"))
       (log-message :debug "Handling new-user request.")
       (if (equal (tbnl:post-parameter "new-user-passwd1")
                  (tbnl:post-parameter "new-user-passwd2"))
           (multiple-value-bind (body status-code)
             (rg-post-json (rg-server tbnl:*acceptor*)
                           "/accounts"
                           :api "auth"
                           :payload `(("username" . ,(tbnl:post-parameter "new-user-uid"))
                                      ("password" . ,(tbnl:post-parameter "new-user-passwd1")))
                           :sessionid sessionid)
             (if (equal status-code 201)
                 (tbnl:redirect "/management?message=New account successfully created")
                 (tbnl:redirect (format nil "/management?message=~A" body))))
           (tbnl:redirect "/management?message=Passwords don't match. Please try again.")))
      ;; POST request to enable/disable accounts
      ((and
         (equal "authenticated" (auth-policy tbnl:*acceptor*))
         (equal (tbnl:request-method*) :POST)
         (tbnl:post-parameter "manage-users"))
       (log-message :debug "Handling request to activate/deactivate accounts")
       (mapcar (lambda (param)
                 (cond
                   ;; Activate an account
                   ((equal (car param) "activate-account")
                    (log-message :debug (format nil "Activating account ~A" (cdr param)))
                    (rg-post-json (rg-server tbnl:*acceptor*)
                                  "/accounts"
                                  :api "auth"
                                  :put-p t
                                  :payload `(("username". ,(cdr param))
                                             ("active" . "true"))
                                  :sessionid sessionid))
                   ;; Deactivate an account
                   ((equal (car param) "deactivate-account")
                    (log-message :debug (format nil "Deactivating account ~A" (cdr param)))
                    (rg-post-json (rg-server tbnl:*acceptor*)
                                  "/accounts"
                                  :api "auth"
                                  :put-p t
                                  :payload `(("username". ,(cdr param))
                                             ("active" . "false"))
                                  :sessionid sessionid))
                   ;; Something else
                   (t (log-message
                        :debug
                        (format nil
                                "Other parameter supplied to activate/deactivate accounts: ~A"
                                param)))))
               (tbnl:post-parameters*))
       (tbnl:redirect "/management"))
      ;; POST request to upload a subschema
      ((and
         (equal (tbnl:request-method*) :POST)
         (or (equal "open" (auth-policy tbnl:*acceptor*))
             (and (equal "authenticated" (auth-policy tbnl:*acceptor*))
                  (equal "ScAdmin" username)))
         (tbnl:post-parameter "schemafile"))
       (log-message :debug "Handling request to upload a subschema")
       (let* ((server (rg-server tbnl:*acceptor*))
              (url (format nil "http://~A:~D~A/"
                           (rg-server-hostname server)
                           (rg-server-port server)
                           (rg-server-schema-base server)))
              (cookie-jar (if sessionid
                              (make-instance
                                'drakma:cookie-jar
                                :cookies (list (make-instance 'drakma:cookie
                                                              :name "sessionid"
                                                              :domain (rg-server-hostname server)
                                                              :path "/"
                                                              :value sessionid)))
                              (make-instance 'drakma:cookie-jar)))
              (create-new-version (if (tbnl:post-parameter "create-new-schema")
                                      "true"
                                      "false")))
         (log-message :debug (format nil "POSTing schema request to URL ~A" url))
         (log-message :debug (format nil "Create new schema version? ~A" create-new-version))
         (multiple-value-bind (body status-code)
           (drakma:http-request
             url
             :form-data t
             :method :POST
             :external-format-in :UTF-8
             :external-format-out :UTF-8
             :cookie-jar cookie-jar
             :real-host (list (rg-server-address server)
                              (rg-server-port server))
             :parameters `(("create" . ,create-new-version)
                           ("schema" . ,(first (tbnl:post-parameter "schemafile")))))
           ;; If it worked, reload the schema so the admin doesn't have to restart the entire instance.
           (when (equal 201 status-code)
             (setf (schema tbnl:*acceptor*)
                   (fetch-schema (rg-server tbnl:*acceptor*))))
           ;; Now display the management page, complete with any reply message from the server.
           (display-mgmt-panel :acceptor tbnl:*acceptor*
                               :sessionid sessionid
                               :username username
                               :message body))))
      ;; POST request with accompanying sessionid cookie
      ;; - defaults to logout.
      ((and sessionid
            (equal "authenticated" (auth-policy tbnl:*acceptor*))
            (equal (tbnl:request-method*) :POST)
            (tbnl:post-parameter "logout"))
       (log-message :debug "Logout request received. Proceeding.")
       (let ((cookie-jar
               (make-instance
                 'drakma:cookie-jar
                 :cookies (list (make-instance 'drakma:cookie
                                               :name "sessionid"
                                               :domain (cookie-domain tbnl:*acceptor*)
                                               :value sessionid)))))
         (rg-delete (rg-server tbnl:*acceptor*)
                    "/auth/v1/sessions"
                    :cookie-jar cookie-jar)
         (tbnl:set-cookie "sessionid"
                          :domain (cookie-domain tbnl:*acceptor*)
                          :path "/"
                          :value ""
                          :expires (- (get-universal-time) 3600))
         (tbnl:redirect "/management")))
      ;; POST request not otherwise handled. Redirect to GET.
      ((equal (tbnl:request-method*) :POST)
       (log-message :debug "Fallback handler for POST requests")
       (tbnl:redirect "/management"))
      ;; GET request with accompanying sessionid cookie.
      ;; Display the management page.
      ((and
         (equal (tbnl:request-method*) :GET)
         ;; Requires that *either* the user is authenticated, *or* authentication is irrelevant.
         (or username
             (equal "open" (auth-policy tbnl:*acceptor*))))
       (log-message :debug "Session detected; displaying management page.")
       (display-mgmt-panel :acceptor tbnl:*acceptor*
                           :sessionid sessionid
                           :username username
                           :message (or (tbnl:get-parameter "message") "")))
      ;; GET, but the site's in read-only mode
      ((and
         (equal "read-only" (auth-policy tbnl:*acceptor*))
         (equal (tbnl:request-method*) :GET))
       (log-message :debug "Management page requested, but site is read-only.")
       (setf (tbnl:content-type*) "text/html")
       (setf (tbnl:return-code*) tbnl:+http-ok+)
       (with-output-to-string (outstr)
         (simple-clwho-layout
           outstr
           :title "Management page"
           :path (list "Management")
           :stylesheets '()
           ;; Is the client authorised to make changes?
           :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                   (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                        sessionid))
                           t)
           :navbar-items (navbar-items tbnl:*acceptor*)
           :content (cl-who:with-html-output-to-string
                      (content-string nil
                                      :prologue nil
                                      :indent t)
                      (:div :class "content"
                       (:p "Site is in read-only mode."))))))
      ;; Default GET case: user needs to log in
      ((equal (tbnl:request-method*) :GET)
       (log-message :debug "No session detected; displaying the login page.")
       (setf (tbnl:content-type*) "text/html")
       (setf (tbnl:return-code*) tbnl:+http-ok+)
       (let ((message (or (tbnl:get-parameter "message")
                          "Sign in, please.")))
         (with-output-to-string (outstr)
           (simple-clwho-layout
             outstr
             :title "Login page"
             :path (list "Login")
             :stylesheets '("login")
             ;; Is the client authorised to make changes?
             :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                     (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                          sessionid))
                             t)
             :navbar-items (navbar-items tbnl:*acceptor*)
             :content (cl-who:with-html-output-to-string
                        (content-string nil
                                        :prologue nil
                                        :indent t)
                        (:div :class "content"
                         (:form :class "formgrid-login"
                          :method "POST"
                          (:div :id "message" (princ message content-string))
                          (:div :id "username-title" "Username: ")
                          (:input :id "username" :name "username")
                          (:div :id "password-title" "Password: ")
                          (:input :id "password" :name "password"
                           :type "password")
                          (:input :id "submit"
                           :type "submit"
                           :value "Log in"))))))))
      ;; Methods that aren't explicitly handled here
      ((not (member (tbnl:request-method*) '(:GET :POST)))
       (method-not-allowed))
      ;; Fallback: request not valid, NFI what to do with it
      (t
        (uri-not-implemented)))))
