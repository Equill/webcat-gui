;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(asdf:defsystem #:webcat-gui
  :license "AGPL-3.0"
  :author "Kat Sebastian <kat@electronic-quill.net>"
  :description "Generates a REST API from a schema defined in Neo4J"
  :depends-on (#:3bmd
               #:3bmd-ext-code-blocks
               #:3bmd-ext-definition-lists
               #:3bmd-ext-tables
               ;#:3bmd-ext-math
               #:cl-ppcre
               #:cl-who
               #:drakma
               #:flexi-streams
               #:hunchentoot
               #:shasht
               #:uuid)
  :components ((:file "package")
               ;; Setup
               (:file "3bmd-tweaks"
                      :depends-on ("package"))
               (:file "config"
                      :depends-on ("package"))
               ;; Mechanisms and utilities
               (:file "structures"
                      :depends-on ("package"))
               (:file "logging"
                      :depends-on ("structures"))
               (:file "errors"
                      :depends-on ("package"))
               (:file "utilities"
                      :depends-on ("errors" "structures"))
               (:file "schema"
                      :depends-on ("utilities"))
               ;; The dispatchers themselves
               (:file "dispatchers"
                      :depends-on ("package"))
               (:file "dispatchers-cl-who"
                      :depends-on ("utilities"))
               (:file "dispatcher-mgmt"
                      :depends-on ("utilities"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-comments"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-create-dependent-item"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-create-item"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-display-item"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-edit-item"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-edit-links"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-files"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-image-gallery"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-searchpage"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               (:file "dispatcher-tasks"
                      :depends-on ("schema"
                                   "dispatchers-cl-who"))
               ;; The appserver
               (:file "hunchentoot"
                      :depends-on ("dispatchers"
                                   "dispatcher-mgmt"
                                   "dispatcher-comments"
                                   "dispatcher-create-dependent-item"
                                   "dispatcher-create-item"
                                   "dispatcher-display-item"
                                   "dispatcher-edit-item"
                                   "dispatcher-edit-links"
                                   "dispatcher-files"
                                   "dispatcher-image-gallery"
                                   "dispatcher-searchpage"
                                   "dispatcher-tasks"))))
