;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;; Create a new resource

(in-package #:webcat-gui)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun create-dependent-item ()
  "Handle creation of dependent resources."
  (let ((sessionid (when (equal "authenticated" (auth-policy tbnl:*acceptor*))
                     (tbnl:cookie-in "sessionid"))))
    (cond
      ;; Display the create-dependent-item form
      ((equal (tbnl:request-method*) :GET)
       (log-message :debug (format nil "Handling create-dependent GET request ~A" (tbnl:request-uri*)))
       ;; Remember to drop the first URI element, which is used for dispatching
       (let ((uri-parts (cdr (get-uri-parts (tbnl:request-uri*)))))
         ;; Was this a canonical URI?
         (if (canonical-path-p (rg-server tbnl:*acceptor*) uri-parts)
           ;; It's canonical. Carry on.
           (progn
             (setf (tbnl:content-type*) "text/html")
             (setf (tbnl:return-code*) tbnl:+http-ok+)
             (with-output-to-string (outstr)
               (simple-clwho-layout
                 outstr
                 :title "Create dependent item"
                 :stylesheets '("create_dependent" "display")
                 :javascripts '("create_dependent")
                 ;; Is the client authorised to make changes?
                 :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                         (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                              sessionid))
                                 t)
                 :navbar-items (navbar-items tbnl:*acceptor*)
                 :content (cl-who:with-html-output-to-string
                            (content-string nil
                                            :prologue nil
                                            :indent t)
                            (:form :action "/create-dependent"
                                   :method "post"
                                   :class "formgrid-create"
                                   :id "createform"
                                   ;; Parent path
                                   (:input :type "hidden"
                                           :name "parent-path"
                                           :value (format nil "~{/~A~}" uri-parts))
                                   ;; UID
                                   (:div :id "header-uid" :class "header" "UID/title:")
                                   (:input :type "text"
                                           :name "ScUID"
                                           :id "input-uid"
                                           :value (or (tbnl:get-parameter "ScUID") ""))
                                   ;; Relationships
                                   (:div :class "header" :id "header-relationships" "Relationship")
                                   (:select :name "relationship" :id "relationships"
                                            :size 15)
                                   ;; Resource-types
                                   (:div :class "header" :id "header-resourcetypes" "Resource type")
                                   (:select :name "resourcetype"
                                            :id "resourcetype"
                                            :size 15)
                                   ;; Javascript-updated attribute management
                                   (:div :class "header" :id "header-attributes" "Attributes")
                                   (:div :class "attrnames" :id "attrnames")
                                   (:div :class "attrvals" :id "attrvals")
                                   ;; Submit button
                                   (:input :type "submit" :value "Create" :id "createbutton"))))))
           ;; URI was not canonical
           (progn
             (log-message :warn "This wasn't a canonical URI")
             (setf (tbnl:content-type*) "text/plain")
             (setf (tbnl:return-code*) tbnl:+http-not-found+)
             "This was not a canonical URI. Please try again with a real one."))))
      ((equal (tbnl:request-method*) :POST)
       (let ((parent-path (tbnl:post-parameter "parent-path"))
             (relationship (tbnl:post-parameter "relationship"))
             (resourcetype (tbnl:post-parameter "resourcetype"))
             (syscat-uid (tbnl:post-parameter "ScUID")))
         (log-message :debug (format nil "Handling create POST request ~A" (tbnl:request-uri*)))
         (cond
           ;; Either read-only mode, or session not authorised
           ((or (equal (auth-policy tbnl:*acceptor*) "read-only")
                (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                     (not sessionid)))
            (return-client-error "Request not permitted."))
           ;; Check for the Syscat UID and resourcetype; if we don't have those, give up now
           ;; Missing both of them
           ((or (not syscat-uid)
                (equal "" syscat-uid)
                (not resourcetype)
                (equal "" resourcetype)
                (not parent-path)
                (equal "" parent-path))
            (progn
              (setf (tbnl:content-type*) "text/plain")
              (setf (tbnl:return-code*) tbnl:+http-bad-request+)
              "The UID, the resourcetype and the parent-path must all be specified"))
           ;; Missing only the UID
           ((or (not syscat-uid)
                (equal syscat-uid ""))
            (setf (tbnl:content-type*) "text/plain")
            (setf (tbnl:return-code*) tbnl:+http-bad-request+)
            "The UID parameter must be supplied")
           ;; Missing only the resourcetype
           ((or (not resourcetype)
                (equal resourcetype ""))
            (setf (tbnl:content-type*) "text/plain")
            (setf (tbnl:return-code*) tbnl:+http-bad-request+)
            "The resourcetype must be specified in the URL")
           ;; Missing the parent-path
           ((or (not parent-path)
                (equal parent-path ""))
            (setf (tbnl:content-type*) "text/plain")
            (setf (tbnl:return-code*) tbnl:+http-bad-request+)
            "The parent-path must be specified in the URL")
           ;; Non-canonical parent path
           ((not (canonical-path-p (rg-server tbnl:*acceptor*) (get-uri-parts parent-path)))
            (setf (tbnl:content-type*) "text/plain")
            (setf (tbnl:return-code*) tbnl:+http-bad-request+)
            "The specified parent path is not canonical.")
           ;; We have all of them; carry on
           (t
             ;; Extract non-empty attributes relevant to this resourcetype
             (let* ((valid-attrnames
                      (mapcar #'name
                              (get-attrs (schema tbnl:*acceptor*) resourcetype)))
                    (validated-attrs
                      (remove-if #'null
                                 (mapcar #'(lambda (param)
                                             (log-message
                                               :debug
                                               (format nil "Validating parameter ~A" (car param)))
                                             (when (and (not (or (null (cdr param))
                                                                 (equal "" (cdr param))))
                                                        (member (car param) valid-attrnames :test #'equal))
                                               param))
                                         (tbnl:post-parameters*)))))
               (log-message :debug (format nil "Validated attributes: ~A" validated-attrs))
               ;; Send the update
               (multiple-value-bind (body status-code)
                 (rg-post-json (rg-server tbnl:*acceptor*)
                               (format nil "~A/~A/~A" parent-path relationship resourcetype)
                               :payload (append `(("ScUID" . ,(sanitise-uid syscat-uid))) validated-attrs)
                               :sessionid sessionid)
                 (log-message :debug (format nil "Server response: ~D - ~A" status-code body))
                 ;; Did it work?
                 (if (and (> status-code 199)
                          (< status-code 300))
                   ;; Happy path
                   (tbnl:redirect (format nil "/display~A/~A/~A/~A"
                                          parent-path relationship resourcetype (sanitise-uid syscat-uid)))
                   ;; Less-happy path
                   (with-output-to-string (outstr)
                     (setf (tbnl:content-type*) "text/html")
                     (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                     (simple-clwho-layout
                       outstr
                       :title (format nil "Failed to create ~A '~A'" resourcetype syscat-uid)
                       :path (cl-ppcre:split "/" parent-path)
                       :stylesheets '("display")
                       :javascripts '("display")
                       ;; Is the client authorised to make changes?
                       :authorised-p (when (or (equal (auth-policy tbnl:*acceptor*) "open")
                                               (and (equal (auth-policy tbnl:*acceptor*) "authenticated")
                                                    sessionid))
                                       t)
                       :navbar-items (navbar-items tbnl:*acceptor*)
                       :content (with-output-to-string (content-string)
                                  (display-default
                                    content-string
                                    `(("Server message" . ,body)))))))))))))
      ;; Fallback: not by this method
      (t (method-not-allowed)))))
