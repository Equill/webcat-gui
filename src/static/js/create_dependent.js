// Declare this as a global variable, because that's how JS rolls
let dependent_rels = {};

// Put all the available relationship types in the select-box.
// Expects the object returned by fetch_dependent_outbound_rels().
function populate_relationships() {
  console.log('Appending relationships to the Select field')
  for (name in dependent_rels) {
    //console.log('Attempting to append relationship ' + name);
    $('select#relationships').append('<option name="' + name + '">' + name + '</option>');
  }
}

// Populate the Resource Type field with the resourcetypes available as targets for the
// selected relationship
function populate_resourcetypes(rel) {
  console.log('Populating the resourcetype select field for relationship ' + rel);
  // It's simpler to just zero out the select-boxes first
  $("div#attrnames").children().remove();
  $("div#attrvals").children().remove();
  $("select#resourcetype").children().remove();
  //clear_resourcetypes();
  // Now add the available resourcetypes
  let add_rtype = function(rtype, index, array) {
      console.log('Adding resourcetype "' + rtype + "' to the resourcetype select field.")
      $("select#resourcetype").append('<option name="' + rtype + '">' + rtype + '</option>');
  }
  console.log('Using data ' + dependent_rels[rel])
  dependent_rels[rel].forEach(add_rtype);
}

function add_dependents_to_obj(data) {
  let adder = function(rel, index, array) {
    console.log('Testing relationship ' + rel['name']);
    // Only take action if this relationship is dependent
    if (rel.reltype == "dependent") {
      console.log('Found dependent relationship ' + rel.name);
      // Report what's there
      console.log('Target types for ' + rel['name'] + ': ' + rel["target-types"])
      // Define these as the target types for this relationship
      dependent_rels[rel.name] = rel["target-types"]
    }
  }
  data.forEach(adder);
}

// Diagnostic function
function enumerate_dependents() {
  console.log('Verifying the contents of dependent_rels:')
  for (let arr in dependent_rels) {
    console.log('Enumerating contents of field ' + arr);
    dependent_rels[arr].forEach(function(data) {
      console.log('Entry: "' + data + "'.");
    });
  }
}

// Sort each of the arrays in an object
function sort_dependent_rels() {
  // Verify what's in the object
  enumerate_dependents();
  // Sort it
  console.log('Sorting the dependent relationships')
  for (let arr in dependent_rels) {
    //console.log('Sorting array entry ' + arr)
    dependent_rels[arr].sort();
  }
  // Verify afterwards
  enumerate_dependents();
}

function fetch_dependents_for_any() {
  console.log("Fetching relationships for resourcetype any.")
  // Query the schema for the "any" resourcetype, and iterate over the attributes described in it.
  $.getJSON(url_schema + 'resourcetypes/' + "any", function(result){
    add_dependents_to_obj(result["relationships"]);
    sort_dependent_rels();
    populate_relationships();
  });
}

// Fetch the available outbound relationships from a resourcetype, and pack them into an object
function fetch_dependents_for_type(sourcetype) {
  console.log('Fetching relationships for resourcetype ' + sourcetype)
  // Query the schema for this resourcetype, and iterate over the attributes described in it.
  $.getJSON(url_schema + 'resourcetypes/' + sourcetype, function(result){
    add_dependents_to_obj(result["relationships"], {});
    fetch_dependents_for_any();
  });
}

// Assemble everything once the document has finished loading
$(document).ready(function(){
  console.log('Loading');
  // Grab the information we need about the resource we're creating dependent things from
  var urlparts = window.location.pathname.split('/');
  var resourcetype = urlparts[urlparts.length - 2];
  var uid = urlparts[urlparts.length - 1];
  // Fetch the information we need about valid outbound dependent relationships
  // and populate the relationship selector
  console.log('Fetching initial data');
  fetch_dependents_for_type(resourcetype);
  // Add filter input fields for each attribute of the
  // selected resource-type, both on page-load...
  // ...and when a new resourcetype is selected
  console.log('Applying onClick function-association');
  $("select#resourcetype").click(function(){
    update_attrs($("select#resourcetype option:selected").val());
  });
  $("select#relationships").click(function(){
    populate_resourcetypes($("select#relationships option:selected").val())
  });
});
