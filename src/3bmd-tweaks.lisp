;   Copyright Bart Botta <00003b at gmail.com>
;   - original implementation of print-tagged-element.
;
;   Copyright Kat Sebastian <kat@electronic-quill.net>
;   - modifications added in this file.
;
;   Published under the MIT License, as per the original.
;   - for details, see MIT-LICENSE in the top-level directory

(in-package #:3bmd)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

;;; Pre-compiled cl-ppcre scanners
;;; Improve performance, and arguably make it more explicit just what is being checked for.

(defparameter *tasks-prefix*
  (cl-ppcre:create-scanner "^Tasks")
  "Link to the Tasks panel.")

(defparameter *gallery-prefix*
  (cl-ppcre:create-scanner "^Gallery")
  "Link to the image gallery.")

(defparameter *prequalified-resource-uri*
  (cl-ppcre:create-scanner "^/display/")
  "Identify a URL that has manually had '/display' prepended by a user.")

(defparameter *resource-uri*
  (cl-ppcre:create-scanner "^/")
  "Assumes that anything that starts with a leading `/` instead of a protocol
   is an internal link to another resource.")


;;; This function is the point of this file:
;;; It takes the above identifiers, and modifies the supplied URL accordingly.

(defun tweak-url (url)
  "Automagically modify links, so the user doesn't have to do it themself."
  (cond
    ;; Link to the Tasks UI, optionally with a filter.
    ((cl-ppcre:scan *tasks-prefix* url)
     (let ((elements (cl-ppcre:split ":" url :limit 2)))
       (format nil "/Tasks~A"
               (if (second elements)
                   (concatenate 'string "?" (second elements))
                   ""))))
    ;; Link to the image gallery, optionally with a filter.
    ((cl-ppcre:scan *gallery-prefix* url)
     (let ((elements (cl-ppcre:split ":" url :limit 2)))
       (format nil "/image-gallery~A"
               (if (second elements)
                   (concatenate 'string "?" (second elements))
                   ""))))
    ;; Pre-qualified path to a resource within this service,
    ;; i.e. a link that was created before this mod was added.
    ((cl-ppcre:scan *prequalified-resource-uri* url)
     url)
    ;; Path to a resource within this service
    ((cl-ppcre:scan *resource-uri* url)
     (format nil "/display~A" url))
    ;; Default case: return the string unmodified
    (t url)))


;;; The rendering function which calls out to `tweak-url`.
;;; Swiped and modified from 3bmd.
;;; Is the reason for this file being under the MIT license.

(defmethod print-tagged-element ((tag (eql :explicit-link)) stream rest)
  "Modified from the original, to auto-modify URLs as required"
  (format stream "<a href=\"~a\" ~@[title=\"~a\"~]>"
          (tweak-url (getf rest :source))
          (escape-string
            (or (getf rest :title) (if *always-title* "" nil))))
  (mapcar (lambda (a) (print-element a stream)) (getf rest :label))
  (format stream "</a>"))
