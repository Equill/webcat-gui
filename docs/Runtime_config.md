Runtime configs
===============

# Environment variables

- `LISTEN_ADDR`
    - The address on which this server should listen.
    - Default: "localhost"
- `LISTEN_PORT`
    - The port on which this server should listen.
    - Default: 8080
- `STATIC_PATH`
    - The path to the site's static assets: CSS, Javascript files, etc.
    - Must include a trailing forward-slash, to denote a directory.
    - Default: "/static/"
- `COOKIE_DOMAIN`
    - Used when returning cookies to the client.
    - Usually, but not always, the same as `SC_HOSTNAME`
- `SC_ADDRESS`
    - The hostname or IP address to use when connecting to Syscat.
    - Unless overridden, `webcat-gui` defaults to using this in the `Host:` header.
    - Default: "localhost"
- `SC_HOSTNAME`
    - Overrides the value of `SC_ADDRESS` when setting the `Host:` header for requests to Syscat.
- `SC_PORT`
    - The port number to use when connecting to Syscat.
    - Default: 4950
