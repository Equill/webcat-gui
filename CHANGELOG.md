# Changelog


## [ Unreleased version ]

### Bugs fixed

- When calling `drakma:http-request` and passing the `real-host` argument, pass it as a `(hostname port) list, instead of just the hostname.


### Changed

- Overhaul the Docker-image build process.
- Bugfix some inconsistencies in the tarball build process.


## [ 0.2.0 ]

### Changed

- BREAKING: Update Restagraph references to Syscat, to match the API update.
- Export the symbol `dockerstart`.
    - Should have been done from the start.
- `create_executable.sh`
    - Simply name the executable `webcatgui`, without any version suffixes.
        - Drop the supporting logic around those suffixes.
    - Configure the executable to invoke `dockerstart` instead of `startup` to stop it exiting immediately after a succcessful startup.
- Rename the directory `local_builds` to `tarball` as it makes a little more sense.


## [ 0.1.4 ]

Mostly internal updates and restructuring, to improve quality and to make future changes easier.

### Changed

- Update the copyright notices, because I figured out I'm trans and I may as well bow to reality. Thus, new name.
- Internal: assume the RG server will always return a list when responding to GET requests.
    - This adapts to a change in RG behaviour.
    - Affects the `display-item` and `edit-item` dispatchers.
- Internal: use `shasht` for JSON decoding, instead of `cl-json`.
    - This means `decode-json-response` returns a hash-table instead of an alist.
    - This is less efficient in terms of pure performance, but is *much* easier to code against.
- Internal: simplify `make-acceptor`.
    - There's already a function `fetch-schema`, so refactor `make-acceptor` to call it instead of duplicating what it does.
- Internal: add canonicalisation to the `create-dependent-item` dispatcher.
- Internal: in `make-acceptor`, use `user-homedir-pathname` to auto-generate a path relative to the current-user's home directory, instead of hard-coding the path to my home directory.
- Management UI: display RG's response to schema updates.
    - Internally, this involved refactoring display of this page into its own function (`display-mgmt-panel`), so it can be called from two places instead of being duplicated.
- Internal: modularise the rendering of the Content section when displaying resources.
    - Instead of using `cond` to dispatch on sections of code within `display-item`, put each renderer into a hash-table so `display-item` can dispatch dynamically on resourcetype, without it needing prior knowledge of what types might be in it.
    - When performing `gethash` on that table, default to the generic renderer in the event of a null result, so the calling code can just invoke the result instead of wrapping its own logic (and duplicated code) around it.


### Added

- Internal: function `canonical-path-p`
- Internal: function `display-mgmt-panel`
- Tooling to deploy a local instance and start/stop it via `systemd`.


### Removed

- Internal: unused function `get-uids`.


## [ 0.1.3 ]

### Bugs fixed

- When creating relationships between two resources, now _all_ relevant relationships are displayed.
    - There was a bug in the regex used by `select-relationships` which filtered out all relationships whose name contained the string "RG_" _anywhere_, instead of only those that started with it.
- Internal: `fetch-available-relationships` assumed that the source-resourcetype is present in the schema.
    - Now it returns `nil` if it isn't.


## [ 0.1.2 ]

## Bugs fixed

- Display of CornellSubNotes on CornellNotes.
- Tasks dispatcher no longer breaks when passed the `uid-regex` argument.
    - In `tasks`, fetch in a simpler way from the parameter-list received by Hunchentoot.
    - In `search-for-tasks`, comma-escape the `uid-regex` symbol when assembling a backtick-quoted alist.


<<<<<<< HEAD
## Changed

- When displaying CornellNotes, filter CornellSubNotes out of the Dependent Resources listing, because they're already incorporated into the page.


## [ 0.1.1 ]

Incrementing the minor version seemed like a good idea.

That is, overdue because 0.0.22 should have been 0.1.0.


## Bugs fixed

- When displaying the resource-title in Wikipages, take a more robust approach.


## [ 0.0.23 ]

Major change: adapt to Restagraph API change, which requires clients to explicitly request user-managed attributes other than `uid`.


## Changed

- Explicitly request attributes where required.
- Make `display-item` fetch the MediaType when displaying an image.
- Improve the way `search-for-resources` invokes Drakma:
    - Pass query parameters via the `:parameters` argument, instead of manually tacking them onto the URL.
- Improve the way `get-uids` calls Drakma.
- When displaying Files, show the MediaType _below_ the file, instead of above it.


### Bugs fixed

- `sessionid` references in the `files` dispatch function.



### Removed

- Tags from the
    - search page
    - image-gallery dispatcher
    - `tasks` and `search-for-tasks` functions


## [ 0.0.22 ]

Release vibe: taking advantage of new functionality available from Restagraph.


### Added

- Record version-comments when updating resources.
- "Powered by Restagraph" footer bar, with hyperlink to the Restagraph site.
- Ability to upload schemas via the Management page.
- `RG_AUTH_POLICY` environment variable is now used to infer the auth policy being applied by the Restagraph server.
    - Default is "open".


### Changed

- Include any-to-any relationships among those presented on the edit-relationships page.
- Tweak the title in the standard header: "Title (resourcetype)" instead of "[resourcetype] Title".
- "Accounts" page is now "Management".
    - It also modifies what's displayed (and accepted) according to the authentication policy and (if relevant) whether there's a valid login session, and whether that's for the "RgAdmin" user.
- Destructive actions (add comment, modify links etc.) are only presented to users when they're authorised to perform those actions.
- The nav-bar auto-detects whether Tasks are defined in the schema, and presents a non-linked Blog header instead of they're not.


### Bugs fixed

- Comments couldn't be submitted for dependent resources.
    - Fix: when generating the form, re-generate a URI for the full path instead of assuming the comment is being added to a primary resource.
- Generate the title-string more robustly, when generating layouts.
    - Check whether there's a resourcetype to format, before trying to format it.


## Ticket-tracking update

In all versions older than 0.0.22, issue references of the form #<number> (e.g. `#28`) referred to tickets on Github.

With the removal of this project from Github, these references are no longer meaningful. They've been retained simply because I've yet to get bored enough to go through and remove them.


## [ 0.0.21 ]

### Bugs fixed

- #28 Image gallery: when images are too narrow, the caption was displayed beside them instead of below the, causing the cell to be too small and the image to poke out the bottom of the cell and overlap the one below it.


### Changed

- Compatible with newly-released Restagraph version 0.0.10.
- #34 Link markup: it's no longer necessary to prepend "/display" to internal links.
    - The renderer now does this automatically.
    - It also automatically avoids prepending a duplicate "/display" prefix, so it's not necessary to update all links immediately.
    - Target URLs of the form `Tasks[{:attribute=value, ...}]` and `Gallery[{:attribute=value, ...}]` are now now automagically rendered as links to the applicable page, with the specified filters applied.
- Image gallery: add outlines to the cells, to better visually group captions with their images.
    - Also drop some ineffective CSS attributes.
