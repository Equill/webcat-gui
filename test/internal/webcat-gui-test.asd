;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(asdf:defsystem #:webcat-gui-test
                :serial t
                :license "AGPL-3.0"
                :author "Kat Sebastian <kat@electronic-quill.net>"
                :description "Test suite for Webcat-GUI internal functions"
                :depends-on (#:webcat-gui
                             #:fiveam)
                :components ((:file "package")
                             (:file "webcat-gui-test")))
