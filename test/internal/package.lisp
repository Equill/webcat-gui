;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(defpackage :webcat-gui-test
  (:use #:cl)
  (:export main))
