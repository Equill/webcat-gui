;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   - for details, see LICENSE.txt in the top-level directory
;   Licensed under the AGPL-3.0 License


;;;; Test suite for Webcat-GUI internal functions

(in-package #:webcat-gui-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite main)
(fiveam:in-suite main)

(defparameter *server*
  (webcat-gui::make-rg-server
    :hostname "192.0.2.1"
    :port 4666))

(fiveam:test
  get-outbound-rels
  "Basic tests for `get-outbound-rels`."
  (let ((initial-version (cdr (assoc :CURRENT-VERSION (webcat-gui::rg-request-json *server* "?version=list" :api "schema"))))
        (results-org (webcat-gui::get-outbound-rels *server* "Organisations")))
    (webcat-gui::log-message :info (format nil "Initial schema version is ~A." initial-version))
    (fiveam:is (not (null results-org)))))
