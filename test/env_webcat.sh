export SC_HOSTNAME='webcat.onfire.onice'
export SC_ADDRESS='webcat.onfire.onice'
export SC_PORT=4949
export COOKIE_DOMAIN='webcat.onfire.onice'
export LISTEN_ADDR='127.0.0.1'
export LISTEN_PORT=8083
export STATIC_PATH='/home/kat/devel/syscat/webcat-gui/src/static/'
export SC_AUTH_POLICY='open'
