# Create a distribution suitable for deployment

# Establish the version suffix
if [[ -z $1 ]]
then
  SUFFIX="snapshot"
else
  SUFFIX=${1}
fi

# Define the name of the directory into which we'll copy the executable
# and its supporting files.
DIRNAME="webcatgui_$SUFFIX"

# Create the executable
./create_executable.sh

# Ensure the builds directory is present
BUILDS_DIR="../../builds"
# It's not there there
if [[ ! -d $BUILDS_DIR ]]
then
  # There's a non-directory file at that path
  if [[ -e $BUILDS_DIR ]]
  then
    echo "Please remove or rename the file at $BUILDS_DIR"
    exit 1
  # Nothing there
  else
    echo "Creating builds directory $BUILDS_DIR - please stand by."
    mkdir $BUILDS_DIR
  fi
fi


# Create the directory for this version if it's not already present.
# If it already exists, assume we're refreshing whatever's there.
if [[ ! -d "$BUILDS_DIR/$DIRNAME" ]]
then
  mkdir "$BUILDS_DIR/$DIRNAME"
fi

# Copy the executable in there
cp "webcatgui" "$BUILDS_DIR/$DIRNAME/"

# Copy the supporting files in there
cp -r ../../src/static "$BUILDS_DIR/$DIRNAME/"
