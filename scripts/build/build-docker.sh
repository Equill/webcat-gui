# Establish the version suffix
if [[ -z $1 ]]
then
  SUFFIX="snapshot"
else
  SUFFIX=${1}
fi

# Copy local dependencies into place
mkdir resources/src
rsync -a --delete-after --exclude='.git' ../../src/ resources/src/webcatgui/
rsync -a --delete-after ~/devel/lisp/3bmd/ resources/src/3bmd/

NOCACHE=false
docker build --no-cache=$NOCACHE --progress=plain -t equill/webcatgui:$SUFFIX ./
