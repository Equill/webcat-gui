sbcl --userinit sbclrc \
    --eval "(ql:quickload :webcat-gui)" \
    --eval "(sb-ext:save-lisp-and-die \"webcatgui\" :executable t :toplevel #'(lambda () (webcat-gui::dockerstart)))"
