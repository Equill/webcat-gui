# webcat-gui

It's a basic web-GUI for [Webcat](https://github.com/equill/webcat), a knowledge-management system (yes, another one!) built on [Syscat](https://codeberg.org/Equill/syscat).

If it looks like it was built by a back-end developer with a history in infrastructure and not in graphic design, UI or UX, there's a good reason for that. It's very much Works For Me™.


# License

Against the unlikely event that somebody else wants to use this code, `webcat-gui` is licensed under AGPL-3.0.


# Installation

Two options are available.

Either way, note that this only covers installation of the GUI server, and assumes you've already separately installed Syscat.


## Docker

Easy method: pull the latest image for `equill/webcatgui` from Docker hub.

Built it yourself: `cd scripts/build && ./build-docker.sh <tag-name>`


## Local install managed by systemd

- Obtain a release, by either method:
  - Download the latest from `https://codeberg.org/Equill/webcat-gui/releases`
  - Build it from source: `cd scripts/tarball && ./release.sh <tag number>`
    - You need SBCL and a recent Quicklisp distribution, plus `tar` and `gzip`.
    - You'll find the resulting tarball in the `releases` subdirectory (`../../releases` relative to `tarball/`).
- Create the `webcat` group and user, assigning that as the primary group for the user.
- Ensure `/opt/webcatgui/` exists and is owned by the `webcat` user.
- Uncompress the tarball into `/opt/webcatgui/`
- Create a `current` symlink to the new directory, e.g. `ln -s /opt/webcatgui/webcatgui-0.1.4 /opt/webcatgui/current`
- Copy `scripts/tarball/webcatgui.service` to `/etc/systemd/system/webcatgui.service` and edit as necessary.
